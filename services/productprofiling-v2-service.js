/**
 * @name productprofiling-v2-service
 * @description This module serve content v2 route product profiling.
 */
'use strict';

const hydraExpress = require('hydra-express');
const hystrixutil = require('@xl/hystrix-util');
const masheryhttp = require('@xl/mashery-http');
const authenticator = require('@xl/txl-remote-authenticate');
const config = require('fwsp-config');
const cfenv = require('cfenv');
const axios = require('axios');
const stringify = require('jsesc');
const jClone = require('@xl/json-clone');
const aes256 = require('@xl/custom-aes256');
const intercomm = require('@xl/cf-intercomm');
const indirectObject = require('@xl/indirect-object-handler');
const rc = require('../constant/RC');
const Moment = require('moment');
const MomentRange = require('moment-range');

const productBenefitService = require('./productbenefit-v2-service');
const apigatehttp = require('@xl/apigate-http');
const { parse } = require('uuid');

const beCipher = new aes256();
beCipher.createCipher(config.pin.backend);


module.exports = {
  getMsisdnCacheToggle: () => {
    return function(req, res) {
      return req.user && req.user.msisdn;
    }
  },
  msisdnCacheOptions : {
    appendKey: (req, res) => {
      return req.user && req.user.msisdn ? req.user.msisdn : req.client.client_id;
    },
    headers: {
      'cache-control': 'no-cache, no-store, must-revalidate',
      'pragma': 'no-cache',
      'expires': 0
    }
  },
  cacheOptions: {
    appendKey: (req, res) => {
      return req.user && req.user.msisdn ? req.user.msisdn : req.client.client_id;
    },
    headers: {
      'cache-control': 'no-cache, no-store, must-revalidate',
      'pragma': 'no-cache',
      'expires': 0
    }
  },
  getCacheDurationInMillis: () => {
    const today = new Date();
    const seconds = today.getSeconds();
    const minutesToSeconds = today.getMinutes() * 60;
    const hoursToSeconds = today.getHours() * 60 * 60;
    const result = hoursToSeconds + minutesToSeconds + seconds;
    return (86400 - result) * 1000;
  },
  checkAccessLevel: (req, res, next) => {
    /*
    req.user = {
      msisdn: '6287787237069',
      parent_account_cd: '90057197'
    }
    */
    const authHeader = req.headers.authorization;
    const { apiName } = req.params;
    const msisdn = req.user ? req.user.msisdn : null;

    if(config.apis[apiName] && (config.apis[apiName].accessLevel === 'public' && (authHeader.startsWith('Basic ') || authHeader.startsWith('Bearer ')) || config.apis[apiName].accessLevel === 'protected' && authHeader.startsWith('Bearer ') && msisdn || (config.apis[apiName].accessLevel === 'private' && authenticator.isInternalAccessor(req)))) {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Check registered API', message: 'API '+ apiName +' exists'}));
      next();
    } else {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Check registered API', message: 'API '+ apiName +' not exists'}));
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'API not found'}));
      res.sendNotFound({code: rc.codes['40'].rc, message: rc.codes['40'].rm});
    }
  },
  getList: (req, res) => {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request for profiling CMS API' }));
    /*
    req.user = {
      msisdn: '6287787237069',
      parent_account_cd: '90057197'
    }
    */
    const lang = req.headers.language;
    const { apiName } = req.params;
    if (apiName === 'productcategoriesv2' || apiName === 'productitemlistv2' || apiName === 'roprogramlist' || apiName === 'roprogramdetail') {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Profiling API found'}));
      const requests = [];
      requests.push(module.exports.getProfilingRule(req));
      requests.push(module.exports.getProductCMS(req, apiName));
      Promise.all(requests).then(results => {
        const profilingRule = results[0] ? results[0] : [] ;
        const products = results[1] ? results[1] : [];
        if (apiName === 'productitemlistv2') {
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Profiling API '+ apiName}));
          module.exports.getProfilingProductItemList(req, res, profilingRule, products);
        } else {
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Profiling API '+ apiName}));
          module.exports.getProfiling(req, res, profilingRule, products);
        }
      }).catch(error => {
        hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call Promise Profiling Product', message: 'Error calling Promise Profiling Product', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
        res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
      });
    } else {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Profiling API Not Found'}));
      res.sendNotFound({errorCode: rc.codes['12'].rc, errorMessage: rc.i18n('12', lang).rm});
    }
  },
  getListV2: (req, res) => {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request for profiling CMS API' }));
    /*
    req.user = {
      msisdn: '6287787237069',
      parent_account_cd: '90057197'
    }
    */
    const lang = req.headers.language;
    const { apiName } = req.params;
    if (apiName === 'productcategoriesv2' || apiName === 'productitemlistv2') {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Profiling API found'}));
      const requests = [];
      requests.push(module.exports.getProfilingRule(req));
      requests.push(module.exports.getSelfProductCMSV2(req, apiName));
      requests.push(module.exports.getTenure(req));
      requests.push(module.exports.getStatusSP(req));
      Promise.all(requests).then(results => {
        const profilingRule = results[0] ? results[0] : [] ;
        const products = results[1] ? results[1] : [];
        const tenure = results[2] ? results[2] : [];
        const statusSP = results[3] ? results[3] : [];
        if (apiName === 'productitemlistv2') {
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Profiling API '+ apiName}));
          module.exports.getProfilingProductItemListV2(req, res, profilingRule, products, tenure, statusSP);
        } else {
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Profiling API '+ apiName}));
          module.exports.getProfilingV2(req, res, profilingRule, products);
        }
      }).catch(error => {
        hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call Promise Profiling Product', message: 'Error calling Promise Profiling Product', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
        res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
      });
    } else {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Profiling API Not Found'}));
      res.sendNotFound({errorCode: rc.codes['12'].rc, errorMessage: rc.i18n('12', lang).rm});
    }
  },
  getProfilingProductItemList: (req, res, profilingRule, products) => {
    const lang = req.headers.language;
    const eligibleProduct = [];
    if (products && products.length > 0) {
      products.forEach(elm => {
        elm.location = 'DEFAULT';
        if (!elm.city_level) {
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'List product', message: 'City profiling empty'}));
          elm.package_appear = 'Berlaku di semua Area';
          eligibleProduct.push(elm);
        } else {
          const localBenefit = [];
          const localValue = elm.city_level.replace(/ /g, '').split('$');

          for(let i = 0; i < localValue.length; i++) {
            const item = localValue[i].split('|');
            localBenefit.push(item);
          }
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Local Benefit Product Detail list', message: 'Local Product Detail', data:localBenefit}));

          //filter product code on profiling rule level
          const matchedProductProfile = profilingRule.filter(i => i.product_code.includes(elm.product_code));
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Local Benefit match profiling rule', message: 'Local Benefit match profiling rule', data:matchedProductProfile}));

          //filter level and city
          const matchedLocalBenefit = [];
          const matchedLocalBenefitProfile = [];
          for (let i=0; i < localBenefit.length; i++){
            for (let j=0; j< matchedProductProfile.length; j++){
              if(localBenefit[i].indexOf(matchedProductProfile[j].level) >= 0) {
                matchedLocalBenefit.push(localBenefit[i]);
              }
            }
          }
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Local Benefit match profile detail', message: 'Local Benefit match profile detail '+elm.product_code, data:matchedLocalBenefit}));

          if (matchedProductProfile && matchedProductProfile.length > 0) {
            for (let i=0; i<matchedProductProfile.length; i++){
              const productCodelist = matchedProductProfile[i].product_code.replace(/ /g, '').split(',') ;
              for (let j=0; j<productCodelist.length; j++){
                if(productCodelist[j] === elm.product_code) {
                  elm.location = matchedProductProfile[i].city;
                  elm.normal_price = matchedLocalBenefit && matchedLocalBenefit.length > 0 ? matchedLocalBenefit[0][3] : elm.normal_price;
                  elm.dompul_price = matchedLocalBenefit && matchedLocalBenefit.length > 0 ? matchedLocalBenefit[0][2] : elm.dompul_price;
                  elm.package_appear = matchedLocalBenefit && matchedLocalBenefit.length > 0 && matchedLocalBenefit[0][1] !== 'null' ? 'Bonus Kuota Lokal di '+matchedProductProfile[i].city : '';
                  eligibleProduct.push(elm);
                }
              }
            }
          }

          delete elm.city_level;
        }
      });
    }
    res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: eligibleProduct});
  },
  getProfilingV2: (req, res, profilingRule, products) => {
    const { apiName } = req.params;
    const lang = req.headers.language;
    const user = req.user ? req.user : {};
    const eligibleProduct = [];
    let product;
    if (products && products.length > 0) {
      products.forEach(elm => {
        elm.location = 'DEFAULT';
        const prodDisplay = isValid(elm.display_start_date, elm.display_end_date);
        const sanityDisplay = isValid(elm.sanity_start_date, elm.sanity_end_date);
        const sanityParentAccountCds = elm.sanity_parent_accountcd ? elm.sanity_parent_accountcd.replace(/ /g, '').split(',') : [];
        if (!prodDisplay){
          if (sanityDisplay && sanityParentAccountCds.includes(user.parent_account_cd) ) {
            eligibleProduct.push(elm);
          }
        } else {
          eligibleProduct.push(elm);
        }
      });
    }
    module.exports.getFilterCityProfilingCategories(req, res, profilingRule, eligibleProduct);
  },
  getFilterCityProfilingCategories: (req, res, profilingRule, products) =>{
    let product;
    const eligibleProduct = [];
    const { apiName } = req.params;
    const lang = req.headers.language;
    if (products && products.length > 0) {
      products.forEach(elm => {
        if (!elm.city_level) {
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'List product', message: 'City profiling empty'}));
          eligibleProduct.push(elm);
        } else{
          const localBenefit = [];
          const localValue = elm.city_level.replace(/ /g, '').split('$');

          for(let i = 0; i < localValue.length; i++) {
            const item = localValue[i].split('|');
            localBenefit.push(item);
          }
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'City level product category list', message: 'City level program category list', data:localBenefit}));

          for (let i=0; i < localBenefit.length; i++){
            for (let j=0; j< profilingRule.length; j++){
              if(localBenefit[i].indexOf(profilingRule[j].level) >= 0) {
                eligibleProduct.push(elm);
                elm.location = profilingRule[j].city;
              }
            }
          }
        }
        delete elm.city_level;
        delete elm.sanity_parent_accountcd;
        delete elm.sanity_start_date;
        delete elm.sanity_end_date;
      });

      const seenNames = {};
      if (apiName === 'productcategoriesv2') {
        product = eligibleProduct.filter(function (currentObject) {
          if (currentObject.product_category in seenNames) {
            return false;
          } else {
            seenNames[currentObject.product_category] = true;
            return true;
          }
        });
      } else {
        product = eligibleProduct.filter(function (currentObject) {
          if (currentObject.news_id in seenNames) {
            return false;
          } else {
            seenNames[currentObject.news_id] = true;
            return true;
          }
        });
      }

    }
    res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: product});
  },
  getProfiling: (req, res, profilingRule, products) => {
    const { apiName } = req.params;
    const lang = req.headers.language;
    const eligibleProduct = [];
    let product;
    if (products && products.length > 0) {
      products.forEach(elm => {
        elm.location = 'DEFAULT';
        if (!elm.city_level) {
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'List product', message: 'City profiling empty'}));
          eligibleProduct.push(elm);
        } else{
          const localBenefit = [];
          const localValue = elm.city_level.replace(/ /g, '').split('$');

          for(let i = 0; i < localValue.length; i++) {
            const item = localValue[i].split('|');
            localBenefit.push(item);
          }
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'City level product category list', message: 'City level program category list', data:localBenefit}));

          for (let i=0; i < localBenefit.length; i++){
            for (let j=0; j< profilingRule.length; j++){
              if(localBenefit[i].indexOf(profilingRule[j].level) >= 0) {
                eligibleProduct.push(elm);
                elm.location = profilingRule[j].city;
              }
            }
          }

          delete elm.city_level;
        }
      });

      const seenNames = {};
      if (apiName === 'productcategoriesv2') {
        product = eligibleProduct.filter(function (currentObject) {
          if (currentObject.product_category in seenNames) {
            return false;
          } else {
            seenNames[currentObject.product_category] = true;
            return true;
          }
        });
      } else {
        product = eligibleProduct.filter(function (currentObject) {
          if (currentObject.news_id in seenNames) {
            return false;
          } else {
            seenNames[currentObject.news_id] = true;
            return true;
          }
        });
      }

    }
    res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: product});
  },
  getProfilingRule: (req) => {
    const { user } = req;
		const envVars = cfenv.getAppEnv();
		const authHeader = req.headers.authorization;
    const environment = !envVars.isLocal ? (envVars.app.space_name == 'prod' ? '' : '-'+ envVars.app.space_name) : (process.env.EKS_ENV && process.env.EKS_ENV !== '' ? process.env.EKS_ENV +'-' : '');
		const apiServiceName = !envVars.isLocal ? 'txl-login-controller-service'+ environment : environment +'txl-login-controller-service';
		const apiNameService = 'profilingrule';
		const apiQuerys = [['authorizationHeader', authHeader]];
    const apiOptions = {
      profilingrule: {
        'url': '/v2/auth/profiling-rule',
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': '${authorizationHeader}'
        },
        circuitBreakerSleepWindowInMilliseconds: 5000,
        circuitBreakerRequestVolumeThreshold: 5,
        circuitBreakerForceOpened: false,
        circuitBreakerForceClosed: false,
        circuitBreakerErrorThresholdPercentage: 60,
        statisticalWindowLength: 10000,
        statisticalWindowNumberOfBuckets: 10,
        requestVolumeRejectionThreshold: 100,
        timeout: 5000
      }
    }
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Get Profiling Rule' }));
    if (process.env.EKS_ENV) {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Check environment', message: 'EKS ' + environment + ' environment' }));
      const options = {
        serviceName: apiServiceName,
        apiName: apiNameService,
        querys: apiQuerys,
        serviceOptions: {
          credentials: {
            url: 'http://' + environment + 'txl-login-controller-service:8080'
          },
          apis: apiOptions
        }
      };
      if (options.serviceOptions && options.serviceOptions.apis && options.serviceOptions.apis[apiNameService]) {
        options.serviceOptions.apis[apiNameService].headers.event_id = req.logId;
        options.serviceOptions.apis[apiNameService].headers.actor = authHeader.startsWith('Bearer ') ? user.msisdn : '';
      }
      return intercomm.request(options).then(response => {
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Calling intercomm login service', message: '/txl-login-controller-service' + environment + '/v2/auth/profiling-rule' }));
        if (response && response.data && response.data.statusCode === 200 && response.data.result ) {
          hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Intercomm login service response', message: 'Get Profiling Rule success', data: response.data }));
          return response.data.result;
        } else {
          hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Intercomm login service response', message: 'Get Profiling Rule empty', data: response.data }));
          hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Get Profiling Rule empty' }));
          return null;
        }
      }).catch(error => {
        hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Calling intercomm login service', message: 'Error calling intercomm login service', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Get Profiling Rule failed' }));
        return null;
      });
    } else {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Check environment', message: 'Local environment' }));
      const options = {
        serviceName: apiServiceName,
        apiName: apiNameService,
        querys: apiQuerys,
        serviceOptions: {
          credentials: {
            url: 'http://localhost:8082'
          },
          apis: apiOptions
        }
      };
      if (options.serviceOptions && options.serviceOptions.apis && options.serviceOptions.apis[apiNameService]) {
        options.serviceOptions.apis[apiNameService].headers.event_id = req.logId;
        options.serviceOptions.apis[apiNameService].headers.actor = authHeader.startsWith('Bearer ') ? user.msisdn : '';
      }
      return intercomm.request(options).then(response => {
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Calling intercomm login service', message: '/txl-login-controller-service' + environment + '/v2/auth/profiling-rule' }));
        if (response && response.data && response.data.statusCode === 200 && response.data.result ) {
          hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Intercomm login service response', message: 'Get Profiling Rule success', data: response.data }));
          return response.data.result;
        } else {
          hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Intercomm login service response', message: 'Get Profiling Rule empty', data: response.data }));
          hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Get Profiling Rule empty' }));
          return null;
        }
      }).catch(error => {
        hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Calling intercomm login service', message: 'Error calling intercomm login service', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Profiling Rule failed' }));
        return null;
      });
    }
  },
  getProductCMS: (req, apiName) => {
    const { user } = req;
    const queryString = req.query;
    const keys = Object.keys(queryString);
    const querys = [];
    for (let i = 0; i < keys.length; i++) {
      const key = keys[i];
      if (!(key === 'parent-account-cd' || key === 'account-cd' || key === 'dealer-id')) {
        querys.push([key, queryString[key]]);
      }
    }
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    querys.push(['parent-account-cd', user.parent_account_cd]);
    querys.push(['account-cd', user.account_cd]);
    querys.push(['dealer-id', user.dealer_id]);
    const fallbackOptions = {};
    const requestOptions = jClone.clone(config.apis[apiName]);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user && user.msisdn ? user.msisdn : null;

    return hystrixutil.request(apiName, requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Call API ' + apiName, message: 'Call API ' + apiName + ' success' }));
      if (Array.isArray(result.data)) {
        let allowedData = [];
        if (result.data.length > 0 && result.data[0].start_display && result.data[0].end_display) {
          result.data.forEach(elm => {
            const prodDisplay = isValid(elm.start_display, elm.end_display);
            const parentAccountCds = elm.parent_account_cd ? elm.parent_account_cd.replace(/ /g, '').split(',') : [user.parent_account_cd];
            const sanityDisplay = isValid(elm.sanity_start_display, elm.sanity_end_display);
            const sanityParentAccountCds = elm.sanity_parent_account_cd ? elm.sanity_parent_account_cd.replace(/ /g, '').split(',') : [];
            const whitelistType = elm.whitelisted_type;
            const filter = whitelistType ? require('../config/filters/' + whitelistType) : null;
            const filterParam = elm.whitelisted_param;
            const isFiltered = filter && filterParam ? filter.whitelist.includes(getRequestParamValueByString(req, filterParam)) && !filter.blacklist.includes(getRequestParamValueByString(req, filterParam)) : true;
            if (((prodDisplay && parentAccountCds.includes(user.parent_account_cd)) || (sanityDisplay && sanityParentAccountCds.includes(user.parent_account_cd))) && isFiltered) {
              //delete elm.start_display;
              //delete elm.end_display;
              delete elm.sanity_start_display;
              delete elm.sanity_end_display;
              delete elm.parent_account_cd;
              delete elm.sanity_parent_account_cd;
              delete elm.whitelisted_type;
              delete elm.whitelisted_param;
              allowedData.push(elm);
              markMCCMPersonalizedBannerAsRead(req, elm, querys);
            }
          });
        } else {
          result.data.forEach(elm => {
            delete elm.start_display;
            delete elm.end_display;
            delete elm.sanity_start_display;
            delete elm.sanity_end_display;
            delete elm.parent_account_cd;
            delete elm.sanity_parent_account_cd;
            delete elm.whitelisted_type;
            delete elm.whitelisted_param;
          });
          allowedData = result.data;
        }
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Response success', data: allowedData }));
        return allowedData;
      } else {
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Response failed' }));
        return null;
      }
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Call API ' + apiName, message: 'Error call API ' + apiName, error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
      return null;
    });
  },
  getProfilingProductItemListV2: (req, res, profilingRule, products, tenure, statusSP) => {
    const lang = req.headers.language;
    const eligibleProduct = [];
    if (products && products.length > 0) {
      products.forEach(elm => {
        elm.location = 'DEFAULT';
        if (!elm.city_level) {
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'List product', message: 'City profiling empty'}));
          elm.package_appear = 'Berlaku di semua Area';
          eligibleProduct.push(elm);
        } else {
          const localBenefit = [];
          const localValue = elm.city_level.replace(/ /g, '').split('$');

          for(let i = 0; i < localValue.length; i++) {
            const item = localValue[i].split('|');
            localBenefit.push(item);
          }
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Local Benefit Product Detail list', message: 'Local Product Detail', data:localBenefit}));

          //filter product code on profiling rule level
          const matchedProductProfile = profilingRule.filter(i => i.product_code.includes(elm.product_code));
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Local Benefit match profiling rule', message: 'Local Benefit match profiling rule', data:matchedProductProfile}));

          //filter level and city
          const matchedLocalBenefit = [];
          const matchedLocalBenefitProfile = [];
          for (let i=0; i < localBenefit.length; i++){
            for (let j=0; j< matchedProductProfile.length; j++){
              if(localBenefit[i].indexOf(matchedProductProfile[j].level) >= 0) {
                matchedLocalBenefit.push(localBenefit[i]);
              }
            }
          }
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Local Benefit match profile detail', message: 'Local Benefit match profile detail '+elm.product_code, data:matchedLocalBenefit}));

          if (matchedProductProfile && matchedProductProfile.length > 0) {
            for (let i=0; i<matchedProductProfile.length; i++){
              const productCodelist = matchedProductProfile[i].product_code.replace(/ /g, '').split(',') ;
              for (let j=0; j<productCodelist.length; j++){
                if(productCodelist[j] === elm.product_code) {
                  elm.location = matchedProductProfile[i].city;
                  elm.normal_price = matchedLocalBenefit && matchedLocalBenefit.length > 0 ? matchedLocalBenefit[0][3] : elm.normal_price;
                  elm.dompul_price = matchedLocalBenefit && matchedLocalBenefit.length > 0 ? matchedLocalBenefit[0][2] : elm.dompul_price;
                  elm.package_appear = matchedLocalBenefit && matchedLocalBenefit.length > 0 && matchedLocalBenefit[0][1] !== 'null' ? 'Bonus Kuota Lokal di '+matchedProductProfile[i].city : '';
                  hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Get Product Item List V2', message: 'Get Product Item list using profiling rule'}));
                  eligibleProduct.push(elm);
                }
              }
            }
          } else {
            hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Get Product Item List V2', message: 'Get Product Item list empty result'}));
            eligibleProduct.push();
          }

          delete elm.city_level;
        }
      });
    }

    // Check Eligible Tenure Package
    const eligibleProductTenure = [];
    if (eligibleProduct && eligibleProduct.length > 0) {
      eligibleProduct.forEach(elm => {
        if (!elm.tenure) {
          eligibleProductTenure.push(elm);
        } else {
          const tenureCMS = parseInt(elm.tenure, 10);
          if (tenure && tenure.errorCode === '00'
            && diffDays(formatDate(tenure.data), formatDate(Date.now())) <= tenureCMS) {
            eligibleProductTenure.push(elm);
          }
        }
      });
    }

    //Check Status SP
    const eligibleProductIDFlag = []
    if (eligibleProduct && eligibleProduct.length > 0) {
      eligibleProduct.forEach(elm => {
        if (statusSP.errorCode === '00') {
          if (statusSP.status === 'INACTIVE' && elm.status_sp === 'yes') {
            eligibleProductIDFlag.push(elm);
          } else if (statusSP.status === 'ACTIVE' && elm.status_sp !== 'yes') {
            eligibleProductIDFlag.push(elm);
          }
        } else {
          eligibleProductIDFlag.push(elm);
        }
      });
    }

    res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: eligibleProductIDFlag });
  },
  getSelfProductCMSV2: (req, apiName) => {
    const { user } = req;
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const authHeader = req.headers.authorization;
    const requestOptions = jClone.clone(config.apis['self'+apiName]);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;
    const querys = [];
    querys.push(['authHeader', authHeader]);
    if (apiName === "productitemlistv2") {
      querys.push(['parent-account-cd', req.query['parent-account-cd']]);
      querys.push(['brand', req.query['brand']]);
      querys.push(['product-category', req.query['product-category']]);
      querys.push(['msisdn-b', req.query['msisdn-b'] ? req.query['msisdn-b'] : '']);
    } else {
      querys.push(['brand', req.query['brand']]);
    }
    return hystrixutil.request('self'+apiName, requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Call API self'+apiName, message: 'Calling API selfself'+apiName+ 'success', data: result.data}));
      const data = result && result.data && result.data.result && result.data.result.data  ? result.data.result.data : [];
      return data;
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call API self'+apiName, message: 'Error calling API self'+apiName, error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
      return [];
    });
  },
  getProductAndIconBenefitv2: (req, res) => {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Request for product benefit profiling API'}));
    const lang = req.headers.language;
    /*
    req.user = {
      msisdn: '6287787237069',
      parent_account_cd: '90057197'
    }
    */
    const promises = [];
    promises.push(module.exports.getProfilingRule(req));
    promises.push(productBenefitService.getIconBenefit(req));
    promises.push(module.exports.getProductBenefit(req));
    Promise.all(promises).then(results => {
      const profilingRule =results[0] ? results[0] : [];
      const iconBenefit = results[1] ? results[1] : [];
      const productBenefit = results[2] ? results[2] : [];
      if (productBenefit && productBenefit.length > 0 && productBenefit[0].city_level) {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Response product and icon benefit using product benefit v2'}));
        const localBenefit = [];
        const localValue = productBenefit[0].city_level.split('$');

        for(let i = 0; i < localValue.length; i++) {
          const item = localValue[i].split('|');
          localBenefit.push(item);
        }
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Local Benefit Product Detail list', message: 'Local Product Detail', data:localBenefit}));

        //filter product code
        const matchedProductProfile = profilingRule.filter(i => i.product_code.includes(productBenefit[0].product_code));
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Local Benefit match profiling rule', message: 'Local Benefit match profiling rule', data:matchedProductProfile}));
        //filter level and city
        const matchedLocalBenefit = [];
        for (let i=0; i < localBenefit.length; i++){
          for (let j=0; j< matchedProductProfile.length; j++){
            if(localBenefit[i].indexOf(matchedProductProfile[j].level) >= 0) {
              matchedLocalBenefit.push(localBenefit[i]);
            }
          }
        }
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Local Benefit match profile detail', message: 'Local Benefit match profile detail', data:matchedLocalBenefit}));

        if (matchedLocalBenefit && matchedLocalBenefit.length > 0) {
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Get product benefit item', message: 'Response use product benefit profiling rule'}));
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Response product and icon benefit using product benefit v2 profiling'}));
          module.exports.getProfilingProductAndIconBenefitv2(productBenefit, matchedProductProfile, matchedLocalBenefit, iconBenefit, req, res);
        } else if (productBenefit[0].brand && productBenefit[0].brand.toLowerCase() === 'xl') {
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Get product benefit item', message: 'Response brand XL use default product because empty profiling rule'}));
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Response product and icon benefit using product benefit default'}));
          module.exports.getProfilingProductAndIconBenefitDefault(productBenefit, iconBenefit, req, res);
        } else {
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Get product benefit item', message: 'Response brand AXIS not use because empty profiling rule'}));
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Response product and icon benefit empty'}));
          res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: []});
        }
      } else {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Get product benefit item', message: 'Response product not contain city level'}));
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Response product and icon benefit using product benefit v1'}));
        productBenefitService.getProductAndIconBenefit(req, res);
      }

    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call Promise Profiling Product Benefit', message: 'Error calling Promise Profiling Product Benefit', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
      res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
    });
  },
  getProductAndIconBenefit: (req, res) => {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Request for product benefit profiling API'}));
    const lang = req.headers.language;
    /*
    req.user = {
      msisdn: '6287787237069',
      parent_account_cd: '90057197'
    }
    */
    module.exports.getProfilingRule(req).then(result => {
      if (result && result.length > 0) {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Get RO Profiling Rule', message: 'Get RO Profiling Rule success'}));
        const profilingRule = result;
        const promises = [];
        promises.push(productBenefitService.getIconBenefit(req));
        promises.push(module.exports.getProductBenefit(req));

        Promise.all(promises).then(results => {
          const allowedData = results.filter(data=> data.length > 0);
          const iconBenefit = allowedData[0] ? allowedData[0] : [];
          const productBenefit = allowedData[1] ? allowedData[1] : [];

          if (productBenefit && productBenefit.length > 0 && productBenefit[0].city_level) {
            hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Response product and icon benefit using product benefit v2'}));
            const localBenefit = [];
            const localValue = productBenefit[0].city_level.split('$');

            for(let i = 0; i < localValue.length; i++) {
              const item = localValue[i].split('|');
              localBenefit.push(item);
            }
            hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Local Benefit Product Detail list', message: 'Local Product Detail', data:localBenefit}));

            //filter product code
            const matchedProductProfile = profilingRule.filter(i => i.product_code.includes(productBenefit[0].product_code));
            hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Local Benefit match profiling rule', message: 'Local Benefit match profiling rule', data:matchedProductProfile}));
            //filter level and city
            const matchedLocalBenefit = [];
            for (let i=0; i < localBenefit.length; i++){
              for (let j=0; j< matchedProductProfile.length; j++){
                if(localBenefit[i].indexOf(matchedProductProfile[j].level) >= 0) {
                  matchedLocalBenefit.push(localBenefit[i]);
                }
              }
            }
            hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Local Benefit match profile detail', message: 'Local Benefit match profile detail', data:matchedLocalBenefit}));

            if (matchedLocalBenefit && matchedLocalBenefit.length > 0) {
              hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Response product and icon benefit using product benefit v2 profiling'}));
              module.exports.getProfilingProductAndIconBenefitv2(productBenefit, matchedProductProfile, matchedLocalBenefit, iconBenefit, req, res);
            } else {
              hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Response product and icon benefit using product benefit v1'}));
              productBenefitService.getProductAndIconBenefit(req, res);
            }
          } else {
            hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Response product and icon benefit using product benefit v1'}));
            productBenefitService.getProductAndIconBenefit(req, res);
          }
        });
      } else {
        module.exports.getProductBenefit(req).then(productBenefitv2 => {
         if (productBenefitv2 && productBenefitv2.length > 0  && productBenefitv2[0].city_level) {
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Get RO Profiling Rule', message: 'Get RO Profiling Rule empty profiling, product benefit v2 exist'}));
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Response product and icon benefit empty result'}));
          //res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: []});
          productBenefitService.getProductAndIconBenefit(req, res);
        } else {
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Get RO Profiling Rule', message: 'Get RO Profiling Rule empty profiling and product v2'}));
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Response product and icon benefit using product benefit v1'}));
          productBenefitService.getProductAndIconBenefit(req, res);
         }
        }).catch(error => {
          hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Get RO Profiling Rule', message: 'Error getting RO Profiling Rule', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Response product and icon benefit using product benefit v1'}));
          productBenefitService.getProductAndIconBenefit(req, res);
        });
      }
    }).catch(error => {
			hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Get RO Profiling Rule', message: 'Error getting RO Profiling Rule', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
			hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Response product and icon benefit using product benefit v1'}));
      productBenefitService.getProductAndIconBenefit(req, res);
		});
  },
  getProfilingProductAndIconBenefit: (productBenefit, matchedProductProfile, matchedLocalBenefit, iconBenefit, req, res) => {
    const lang = req.headers.language;
    const idNewBenefits = [];
    const enNewBenefits = [];

    const normalPrice = matchedLocalBenefit[0][3] ? matchedLocalBenefit[0][3] : '' ;
    const dompulPrice = matchedLocalBenefit[0][2] ? matchedLocalBenefit[0][2] : '' ;

    productBenefit[0].normal_price = normalPrice;
    productBenefit[0].dompul_price = dompulPrice;

    const productBenefitId =  productBenefit[0].product_benefit_id.filter(word => !word.includes('Bonus Kuota Internet'));
    const productBenefitEn =  productBenefit[0].product_benefit_en.filter(word => !word.includes('Bonus Kuota Internet'));

    const localBenefitProduct = matchedLocalBenefit[0][1] ? matchedLocalBenefit[0][1] : '' ;

    if (!localBenefitProduct.includes('null')) {
      productBenefitId.push(localBenefitProduct);
      productBenefitEn.push(localBenefitProduct);
    }

    for(let i = productBenefitId.length - 1; i >= 0; i--) {
      const idBenefit = productBenefitId[i];
      const enBenefit = productBenefitEn[i];

      let matchedIdIcon = '';
      let matchedEnIcon = '';
      for(let j = 0; j < iconBenefit.length; j++) {
        const icon = iconBenefit[j];
        for(let k = 0; k < icon.keyword.length; k++) {
          if(idBenefit.toLowerCase().indexOf(icon.keyword[k]) >= 0) {
            matchedIdIcon = icon.icon;
          }
          if(enBenefit.toLowerCase().indexOf(icon.keyword[k]) >= 0) {
            matchedEnIcon = icon.icon;
          }
        }
      }

      idNewBenefits.push({wording: idBenefit.includes('Kuota Lokal') || idBenefit.includes('Kuota lokal') ? idBenefit.replace(/kuota lokal/gi,'Berlaku di '+matchedProductProfile[0].city) : idBenefit, icon: matchedIdIcon});
      enNewBenefits.push({wording: enBenefit.includes('Kuota Lokal') || enBenefit.includes('Kuota lokal') ? enBenefit.replace(/kuota lokal/gi,'Berlaku di '+matchedProductProfile[0].city) : enBenefit, icon: matchedEnIcon});
    }

    productBenefit[0].product_benefit_id = idNewBenefits;
    productBenefit[0].product_benefit_en = enNewBenefits;
    delete  productBenefit[0].city_level;

    res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: productBenefit});
  },
  getProfilingProductAndIconBenefitv2: (productBenefit, matchedProductProfile, matchedLocalBenefit, iconBenefit, req, res) => {
    const lang = req.headers.language;
    const idNewBenefits = [];
    const enNewBenefits = [];

    const normalPrice = matchedLocalBenefit[0][3] ? matchedLocalBenefit[0][3] : '' ;
    const dompulPrice = matchedLocalBenefit[0][2] ? matchedLocalBenefit[0][2] : '' ;

    productBenefit[0].normal_price = normalPrice;
    productBenefit[0].dompul_price = dompulPrice;

    const productBenefitId =  productBenefit[0].product_benefit_id.filter(word => !word.includes('Berdasarkan area'));
    const productBenefitEn =  productBenefit[0].product_benefit_en.filter(word => !word.includes('Berdasarkan area'));

    const localBenefitProduct = matchedLocalBenefit[0][1] ? matchedLocalBenefit[0][1] : '' ;

    if (!localBenefitProduct.includes('null')) {
      const localBenefitArr = localBenefitProduct.split('#');
      for(let i = 0; i < localBenefitArr.length; i++) {
        productBenefitId.push(localBenefitArr[i]);
        productBenefitEn.push(localBenefitArr[i]);
      }
    }

    for(let i = productBenefitId.length - 1; i >= 0; i--) {
      const idBenefit = productBenefitId[i];
      const enBenefit = productBenefitEn[i];

      let matchedIdIcon = '';
      let matchedEnIcon = '';
      for(let j = 0; j < iconBenefit.length; j++) {
        const icon = iconBenefit[j];
        for(let k = 0; k < icon.keyword.length; k++) {
          if(idBenefit.toLowerCase().indexOf(icon.keyword[k]) >= 0) {
            matchedIdIcon = icon.icon;
          }
          if(enBenefit.toLowerCase().indexOf(icon.keyword[k]) >= 0) {
            matchedEnIcon = icon.icon;
          }
        }
      }

      idNewBenefits.push({wording: idBenefit.includes('Kuota Lokal') || idBenefit.includes('Kuota lokal') ? idBenefit.replace(/kuota lokal/gi,'Berlaku di '+matchedProductProfile[0].city) : idBenefit, icon: matchedIdIcon});
      enNewBenefits.push({wording: enBenefit.includes('Kuota Lokal') || enBenefit.includes('Kuota lokal') ? enBenefit.replace(/kuota lokal/gi,'Berlaku di '+matchedProductProfile[0].city) : enBenefit, icon: matchedEnIcon});
    }

    productBenefit[0].product_benefit_id = idNewBenefits;
    productBenefit[0].product_benefit_en = enNewBenefits;
    delete  productBenefit[0].city_level;

    res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: productBenefit});
  },
  getProfilingProductAndIconBenefitDefault: (productBenefit, iconBenefit, req, res) => {
    const lang = req.headers.language;
    const idNewBenefits = [];
    const enNewBenefits = [];

    const localBenefit = [];
    const localValue = productBenefit[0].city_level.split('$');

    for(let i = 0; i < localValue.length; i++) {
      const item = localValue[i].split('|');
      localBenefit.push(item);
    }

    const defaultLocalBenefit = [];
    for (let i=0; i < localBenefit.length; i++){
      if (localBenefit[i].indexOf('L99') >= 0) {
        defaultLocalBenefit.push(localBenefit[i]);
      }
    }

    if (defaultLocalBenefit && defaultLocalBenefit.length > 0) {
      const normalPrice = defaultLocalBenefit[0][3] ? defaultLocalBenefit[0][3] : '' ;
      const dompulPrice = defaultLocalBenefit[0][2] ? defaultLocalBenefit[0][2] : '' ;

      productBenefit[0].normal_price = normalPrice;
      productBenefit[0].dompul_price = dompulPrice;

      const productBenefitId =  productBenefit[0].product_benefit_id.filter(word => !word.includes('Berdasarkan area'));
      const productBenefitEn =  productBenefit[0].product_benefit_en.filter(word => !word.includes('Berdasarkan area'));

      const localBenefitProduct = defaultLocalBenefit[0][1] ? defaultLocalBenefit[0][1] : '' ;

      if (!localBenefitProduct.includes('null')) {
        const localBenefitArr = localBenefitProduct.split('#');
        for(let i = 0; i < localBenefitArr.length; i++) {
          productBenefitId.push(localBenefitArr[i]);
          productBenefitEn.push(localBenefitArr[i]);
        }
      }

      for(let i = productBenefitId.length - 1; i >= 0; i--) {
        const idBenefit = productBenefitId[i];
        const enBenefit = productBenefitEn[i];

        let matchedIdIcon = '';
        let matchedEnIcon = '';
        for(let j = 0; j < iconBenefit.length; j++) {
          const icon = iconBenefit[j];
          for(let k = 0; k < icon.keyword.length; k++) {
            if(idBenefit.toLowerCase().indexOf(icon.keyword[k]) >= 0) {
              matchedIdIcon = icon.icon;
            }
            if(enBenefit.toLowerCase().indexOf(icon.keyword[k]) >= 0) {
              matchedEnIcon = icon.icon;
            }
          }
        }

        idNewBenefits.push({wording: idBenefit.includes('Kuota Lokal') || idBenefit.includes('Kuota lokal') ? idBenefit.replace(/kuota lokal/gi,'Berlaku di Area Tertentu') : idBenefit, icon: matchedIdIcon});
        enNewBenefits.push({wording: enBenefit.includes('Kuota Lokal') || enBenefit.includes('Kuota lokal') ? enBenefit.replace(/kuota lokal/gi,'Berlaku di Area Tertentu') : enBenefit, icon: matchedEnIcon});
      }

      productBenefit[0].product_benefit_id = idNewBenefits;
      productBenefit[0].product_benefit_en = enNewBenefits;
      delete  productBenefit[0].city_level;

      res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: productBenefit});
    } else {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Default program not set, use product item benefit v1'}));
      productBenefitService.getProductAndIconBenefit(req, res);
    }
  },
  getProductBenefit: (req) => {
    const { user } = req;
    const authHeader = req.headers.authorization;
    const productCode = req.query['product-code'];
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const requestOptions = jClone.clone(config.apis['selfproductbenefitv2']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = authHeader.startsWith('Bearer ') ? user.msisdn : '';
    const querys = [];
    querys.push(['authHeader', authHeader]);
    querys.push(['productCode', productCode]);
    return hystrixutil.request('selfproductbenefitv2', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Call product benefit API', message: 'Calling product benefit v2 API success', data: result.data}));
      const data = result && result.data && result.data.result && result.data.result.data ? result.data.result.data : [];
      return data;
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call product benefit API', message: 'Error calling product benefit v2 API', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
      return [];
    });
  },
  getTenure: (req) => {
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const bypassForce = req.headers['x-apicache-bypass'] ? req.headers['x-apicache-bypass'] : 'false';
    const { user } = req;
    const bMsisdn = req.query['msisdn-b'] ? req.query['msisdn-b'] : '';
    const authHeader = req.headers.authorization;
    const requestOptions = jClone.clone(config.apis['selfgetsubscriberinfo']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;
    const querys = [];
    querys.push(['msisdn', bMsisdn]);
    querys.push(['authHeader', authHeader]);
    querys.push(['bypassForce', bypassForce]);
    if (bMsisdn) {
      return hystrixutil.request('selfgetsubscriberinfo', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
        if (result.data.result.data && result.data.result.data.IR_DATE) {
          return { errorCode: '00', data: formatDate(result.data.result.data.IR_DATE) };
        } else {
          return { errorCode: '01', data: [] };
        }
      }).catch(error => {
        hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Get Subscriber Info', message: 'Error Get Subscriber Info', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
        return { errorCode: '01', data: [] };
      });
    } else {
      return { errorCode: '01', data: [] };
    }
  },
  getStatusSP: (req) => {
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const { user } = req;
    const bMsisdn = req.query['msisdn-b'] ? req.query['msisdn-b'] : '';
    const authHeader = req.headers.authorization;
    const requestOptions = jClone.clone(config.apis['selfgetstatussp']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;
    const querys = [];
    querys.push(['msisdn', bMsisdn]);
    querys.push(['authHeader', authHeader]);
    if (bMsisdn) {
      return hystrixutil.request('selfgetstatussp', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
        if (result.data.result.data && result.data.result.data.status === "ACTIVE") {
          return { errorCode: '00', status: 'ACTIVE' };
        } else if (result.data.result.data && result.data.result.data.status === "INACTIVE") {
          return { errorCode: '00', status: 'INACTIVE' };
        } else {
          return { errorCode: '01', status: '' };
        }
      }).catch(error => {
        hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Check Status SP', message: 'Error Check Status SP', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
        return { errorCode: '01', status: '' };
      });
    } else {
      return { errorCode: '01', status: '' };
    }
  }
}
function isValid(startDate, endDate) {
  let current = (new Date()).toISOString();
  current = current.substring(0, 19).replace(/[-:T]/g, '');
  const start = startDate ? startDate.substring(0, 19).replace(/[-:T]/g, '') : current;
  const end = endDate ? endDate.substring(0, 19).replace(/[-:T]/g, '') : current;
  if (current >= start && current <= end) {
    return true;
  } else {
    return false;
  }
}

function getRequestParamValueByString(req, paramPath) {
  paramPath = paramPath.startsWith('req.') ? paramPath.substring(4) : paramPath;
  const paramPaths = paramPath.split('.');
  let temp = {};
  for (let i = 0; i < paramPaths.length; i++) {
    if (i === 0) {
      temp = req[paramPaths[i]];
    } else {
      temp = temp[paramPaths[i]];
    }
  }
  return temp;
}

function markMCCMPersonalizedBannerAsRead(req, elm, querys) {
  const user = req.user ? req.user : {};
  let isOfferIdExist = false;
  for (let i = 0; i < querys.length; i++) {
    if (querys[i].includes('offer-id')) {
      isOfferIdExist = true;
      break;
    }
  }
  if (isOfferIdExist && elm.mccm_banner && elm.mccm_banner === 'yes') {
    const fallbackOptions = {};
    const requestOptions = jClone.clone(config.apis['mccmmarkasread']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;
    querys.push(['msisdn', user.msisdn]);
    hystrixutil.request('mccmmarkasread', requestOptions, apigatehttp.request, fallbackOptions, fallback, querys).then(response => {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Call API MCCM callback', message: 'Calling API MCCM callback success' }));
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Call API MCCM callback', message: 'Error calling API MCCM callback', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
    });
  }
}

function formatDate(date) {
  const moment = MomentRange.extendMoment(Moment);
  moment.locale('en');
  return moment(date).format('MM/DD/YYYY');
}

function diffDays(start, end) {
  const date1 = new Date(start);
  const date2 = new Date(end);
  const oneDay = 1000 * 60 * 60 * 24;
  const diffInTime = date2.getTime() - date1.getTime();
  const diffInDays = Math.round(diffInTime / oneDay);
  return diffInDays;
}