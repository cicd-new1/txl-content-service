/**
 * @name content-v2-service
 * @description This module serve content v2 route.
 */
'use strict';

const hydraExpress = require('hydra-express');
const hystrixutil = require('@xl/hystrix-util');
const config = require('fwsp-config');
const axios = require('axios');
const stringify = require('jsesc');
const jClone = require('@xl/json-clone');
const rc = require('../constant/RC');

module.exports = {
  getProductAndIconBenefit: (req, res) => {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Request for product benefit API'}));
    /*
    req.user = {
      msisdn: '6287787237069',
      parent_account_cd: '90057197'
    }
    */
    const lang = req.headers.language;
    const requests = [];
    requests.push(module.exports.getProductBenefit(req, res));
    requests.push(module.exports.getIconBenefit(req, res));
    Promise.all(requests).then(results => {
      const productBenefit = results[0] ? results[0] : [];
      const iconBenefit = results[1] ? results[1] : [];
      const idNewBenefits = [];
      const enNewBenefits = [];
      for(let i = productBenefit[0].product_benefit_id.length - 1; i >= 0; i--) {
        const idBenefit = productBenefit[0].product_benefit_id[i];
        const enBenefit = productBenefit[0].product_benefit_en[i];
        let matchedIdIcon = '';
        let matchedEnIcon = '';
        for(let j = 0; j < iconBenefit.length; j++) {
          const icon = iconBenefit[j];
          for(let k = 0; k < icon.keyword.length; k++) {
            if(idBenefit.toLowerCase().indexOf(icon.keyword[k]) >= 0) {
              matchedIdIcon = icon.icon;
            }
            if(enBenefit.toLowerCase().indexOf(icon.keyword[k]) >= 0) {
              matchedEnIcon = icon.icon;
            }
          }
        }
        idNewBenefits.push({wording: idBenefit, icon: matchedIdIcon});
        enNewBenefits.push({wording: enBenefit, icon: matchedEnIcon});
      }
      productBenefit[0].product_benefit_id = idNewBenefits;
      productBenefit[0].product_benefit_en = enNewBenefits;
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Get product benefit success'}));
      res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: productBenefit});
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call API combined news homepage', message: 'Error calling API combined news homepage', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
      res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
    });
  },
  getProductBenefit: (req, res) => {
    const { user } = req;
    const authHeader = req.headers.authorization;
    const productCode = req.query['product-code'];
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const requestOptions = jClone.clone(config.apis['selfproductbenefit']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = authHeader.startsWith('Bearer ') ? user.msisdn : '';
    const querys = [];
    querys.push(['authHeader', authHeader]);
    querys.push(['productCode', productCode]);
    return hystrixutil.request('selfproductbenefit', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Call product benefit API', message: 'Calling product benefit API success', data: result.data}));
      const data = result && result.data && result.data.result && result.data.result.data ? result.data.result.data : [];
      return data;
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call product benefit API', message: 'Error calling product benefit API', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
      return [];
    });
  },
  getIconBenefit: (req, res) => {
    const { user } = req;
    const authHeader = req.headers.authorization;
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const requestOptions = jClone.clone(config.apis['selficonbenefit']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = authHeader.startsWith('Bearer ') ? user.msisdn : '';
    const querys = [];
    querys.push(['authHeader', authHeader]);
    return hystrixutil.request('selficonbenefit', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Call icon benefit API', message: 'Calling icon benefit API success', data: result.data}));
      const data = result && result.data && result.data.result && result.data.result.data ? result.data.result.data : [];
      return data;
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call icon benefit API', message: 'Error calling icon benefit API', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
      return [];
    });
  }
}