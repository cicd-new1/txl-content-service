/**
 * @name personalhomepage-v2-service
 * @description This module serve content v2 route.
 */
'use strict';

const hydraExpress = require('hydra-express');
const hystrixutil = require('@xl/hystrix-util');
const masheryhttp = require('@xl/mashery-http');
const config = require('fwsp-config');
const axios = require('axios');
const stringify = require('jsesc');
const jClone = require('@xl/json-clone');
const aes256 = require('@xl/custom-aes256');
const rc = require('../constant/RC');
const apigatehttp = require('@xl/apigate-http');

const beCipher = new aes256();
beCipher.createCipher(config.pin.backend);

module.exports = {
  getPersonalHomepage: (req, res) => {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Request for personal homepage API'}));
    /*
    req.user = {
      msisdn: '6287787237069',
      parent_account_cd: '90057197'
    }
    */
    const lang = req.headers.language;
    let maxBanner = req.query.maxbanner ? req.query.maxbanner : 6;
    const data = [];
    const requests = [];
    requests.push(module.exports.getCMSPersonalHomepage(req, res));
    requests.push(module.exports.getMCCMPersonalHomepage(req, res));
    Promise.all(requests).then(results => {
      const cmsBanners = results[0] ? results[0] : [];
      const mccmBanners = results[1] ? results[1] : [];
      const temp = [...cmsBanners, ...mccmBanners];
      temp.sort((a,b) => (a.banner_position > b.banner_position) ? 1 : ((b.banner_position > a.banner_position) ? -1 : 0));
      maxBanner = maxBanner > temp.length ? temp.length : maxBanner;
      for(let i = 0; i < maxBanner; i++) {
        data.push(temp[i]);
      }
      res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data});
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call API combined news homepage', message: 'Error calling API combined news homepage', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
      res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
    });
  },
  cacheOptions: {
    appendKey: (req, res) => {
      return req.user && req.user.msisdn ? req.user.msisdn : req.client.client_id;
    },
    headers: {
      'cache-control': 'no-cache, no-store, must-revalidate',
      'pragma': 'no-cache',
      'expires': 0
    }
  },
  getMsisdnCacheToggle: () => {
    return function(req, res) {
      return req.user && req.user.msisdn;
    }
  },
  getCMSPersonalHomepage: (req, res, newsId, offerId) => {
    const { user } = req;
    const authHeader = req.headers.authorization;
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    if(newsId) {
      const requestOptions = jClone.clone(config.apis['selfmccmnewshomepage']);
      requestOptions.headers.event_id = req.logId;
      requestOptions.headers.actor = user.msisdn;
      const querys = [];
      querys.push(['news-id', newsId]);
      querys.push(['authHeader', authHeader]);
      return hystrixutil.request('selfmccmnewshomepage', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Call API news detail homepage', message: 'Calling API news detail homepage success'}));
        const data = result && result.data && result.data.result && result.data.result.data && result.data.result.data.length > 0 ? result.data.result.data[0] : [];
        data.offer_id = offerId;
        return data;
      }).catch(error => {
        hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call API news detail homepage', message: 'Error calling API news detail homepage', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
        return [];
      });
    } else {
      const requestOptions = jClone.clone(config.apis['selfnewshomepage']);
      requestOptions.headers.event_id = req.logId;
      requestOptions.headers.actor = user.msisdn;
      const querys = [];
      querys.push(['authHeader', authHeader]);
      return hystrixutil.request('selfnewshomepage', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Call API news homepage', message: 'Calling API news homepage success', data: result.data}));
        const data = result && result.data && result.data.result && result.data.result.data ? result.data.result.data : [];
        return data;
      }).catch(error => {
        hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call API news homepage', message: 'Error calling API news homepage', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
        return [];
      });
    }
  },
  getMCCMPersonalHomepage: (req, res) => {
    const { user } = req;
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const requestOptions = jClone.clone(config.apis['nextbestoffer']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;
    const querys = [];
    querys.push(['msisdn', user.msisdn]);
    querys.push(['maxOffer', 6]);
    return hystrixutil.request('nextbestoffer', requestOptions, apigatehttp.request, fallbackOptions, fallback, querys).then(result => {
      const offers = result && result.data && result.data.offers && result.data.offers.offer ? result.data.offers.offer : [];
      const homepageBannerPromises = [];
      if(Array.isArray(offers)) {
        for(let i=0; i<offers.length; i++) {
          const offer = offers[i];
          const offerId = offer.id;
          let newsId = null;
          const channelAttributes = offer.channel_attributes.channel_attribute;
          for(let j=0; j<channelAttributes.length; j++) {
            const attribute = channelAttributes[j];
            if(attribute.name === 'DOMPUL_BANNER_ID') {
              newsId = attribute.value;
              break;
            }
          }
          if(newsId) {
            homepageBannerPromises.push(module.exports.getCMSPersonalHomepage(req, res, newsId, offerId));
          }
        }
      } else {
        const offerId = offers.id;
        let newsId = null;
        const channelAttributes = offers.channel_attributes.channel_attribute;
        for(let j=0; j<channelAttributes.length; j++) {
          const attribute = channelAttributes[j];
          if(attribute.name === 'DOMPUL_BANNER_ID') {
            newsId = attribute.value;
            break;
          }
        }
        if(newsId) {
          homepageBannerPromises.push(module.exports.getCMSPersonalHomepage(req, res, newsId, offerId));
        }
      }
      return Promise.all(homepageBannerPromises).then(banners => {
        const resultBanners = [];
        for(let i=0; i<banners.length; i++) {
          if(banners[i] && banners[i].news_id) {
            resultBanners.push(banners[i]);
          }
        }
        return resultBanners;
      });
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call API news homepage MCCM', message: 'Error calling API news homepage MCCM', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
      return [];
    });
  }
}