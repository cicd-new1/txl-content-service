/**
 * @name productcatalog-v2-service
 * @description This module serve content v2 route product catalog.
 */
'use strict';

const hydraExpress = require('hydra-express');
const hystrixutil = require('@xl/hystrix-util');
const masheryhttp = require('@xl/mashery-http');
const config = require('fwsp-config');
const axios = require('axios');
const stringify = require('jsesc');
const jClone = require('@xl/json-clone');
const aes256 = require('@xl/custom-aes256');
const rc = require('../constant/RC');
const uuidv1 = require('uuid/v1');
const apigatehttp = require('@xl/apigate-http');


const beCipher = new aes256();
beCipher.createCipher(config.pin.backend);

module.exports = {
  cacheOptions: {
    appendKey: (req, res) => {
      return req.user && req.user.msisdn ? req.user.msisdn : req.client.client_id;
    },
    headers: {
      'cache-control': 'no-cache, no-store, must-revalidate',
      'pragma': 'no-cache',
      'expires': 0
    }
  },
  getMsisdnCacheToggle: () => {
    return function (req, res) {
      return req.user && req.user.msisdn;
    }
  },
  getProductCatalog: (req, res) => {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request for product catalog API' }));
    /*
    req.user = {
      msisdn: '6287787237069',
      parent_account_cd: '90057197'
    }
    */
    const lang = req.headers.language;
    const { apiName } = req.params;
    const requests = [];
    requests.push(module.exports.doGetProductCatalog(req, apiName));
    Promise.all(requests).then(results => {
      let product = results[0] ? results[0] : [];

      //  remove duplicates check by product_code
      const seenNames = {};
      product = product.filter(function (currentObject) {
        if (currentObject.product_code in seenNames) {
          return false;
        } else {
          seenNames[currentObject.product_code] = true;
          return true;
        }
      });

      product.sort((a, b) => {
        if (a.product_code < b.product_code)
          return -1;
        if (a.product_code > b.product_code)
          return 1;
        return 0;
      });

      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Get product catalog success' }));
      res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: product });
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Call API product catalog', message: 'Error calling API product catalog', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
      res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
    });
  },
  getProductSearch: (req, res) => {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request for product catalog API' }));
    /*
    req.user = {
      msisdn: '6287787237069',
      parent_account_cd: '90057197'
    }
    */
    const lang = req.headers.language;
    const requests = [];
    requests.push(module.exports.doGetProductSearch(req));
    Promise.all(requests).then(results => {
      let product = results[0] ? results[0] : [];

      //  remove duplicates check by product_code
      const seenNames = {};
      product = product.filter(function (currentObject) {
        if (currentObject.product_code in seenNames) {
          return false;
        } else {
          seenNames[currentObject.product_code] = true;
          return true;
        }
      });

      product.sort((a, b) => {
        if (a.product_code < b.product_code)
          return -1;
        if (a.product_code > b.product_code)
          return 1;
        return 0;
      });

      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Get product catalog success' }));
      res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: product });
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Call API product catalog', message: 'Error calling API product catalog', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
      res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
    });
  },
  doGetProductCatalog: (req, apiName) => {
    return module.exports.doGetXboxIds(req).then(xboxIds => {
      if (xboxIds && xboxIds.length > 0) {
        const xboxIdProfile = xboxIds.toString().split(",");
        const promises = [];
        promises.push(module.exports.getProductCatalogCMS(req, apiName));
        return Promise.all(promises).then(results => {
          const products = results[0] ? results[0] : [];
          const existingProduct = [];
          const eligibleProduct = [];
          if (products && products.length > 0) {
            products.forEach(elm => {
              if (!elm.xboxid) {
                existingProduct.push(elm);
              } else {
                const xboxIdProduct = elm.xboxid.replace(/ /g, '').split(',');
                for (let i = 0; i < xboxIdProduct.length; i++) {
                  if (xboxIdProfile.includes(xboxIdProduct[i])) {
                    eligibleProduct.push(elm);
                    break;
                  }
                }
              }
            });
          }
          const data = [...existingProduct, ...eligibleProduct];
          return data;
        });
      }
    });
  },
  doGetProductSearch: (req) => {
    return module.exports.doGetXboxIds(req).then(xboxIds => {
      if (xboxIds && xboxIds.length > 0) {
        const xboxIdProfile = xboxIds.toString().split(",");
        const promises = [];
        promises.push(module.exports.getProductSearchCMS(req));
        return Promise.all(promises).then(results => {
          const products = results[0] ? results[0] : [];
          const existingProduct = [];
          const eligibleProduct = [];
          if (products && products.length > 0) {
            products.forEach(elm => {
              if (!elm.xboxid) {
                existingProduct.push(elm);
              } else {
                const xboxIdProduct = elm.xboxid.replace(/ /g, '').split(',');
                for (let i = 0; i < xboxIdProduct.length; i++) {
                  if (xboxIdProfile.includes(xboxIdProduct[i])) {
                    eligibleProduct.push(elm);
                    break;
                  }
                }
              }
            });
          }
          const data = [...existingProduct, ...eligibleProduct];
          return data;
        });
      }
    });
  },
  getProductSearchCMS: (req) => {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request for product search API' }));
    const { user } = req;
    const fallbackOptions = {};
    let brand = 'XL';
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const requestOptions = jClone.clone(config.apis['prefix']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;
    return hystrixutil.request('prefix', requestOptions, apigatehttp.request, fallbackOptions, fallback, [['msisdn', req.params.msisdn]]).then(result => {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Call API prefix', message: 'Call API prefix success' }));
      brand = result && result.data && result.data && result.data.owner ? result.data.owner : 'XL';
      return module.exports.filterProductSearch(req, brand);
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Call API prefix', message: 'Error calling API prefix', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
      return module.exports.filterProductSearch(req, brand);
    });
  },
  filterProductSearch: (req, brand) => {
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const lang = req.headers.language;
    const { user } = req;
    const requestOptions = jClone.clone(config.apis['productsearch']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;
    return hystrixutil.request('productsearch', requestOptions, axios, fallbackOptions, fallback, [['keyword', req.params.keyword]]).then(result => {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Call API product search', message: 'Call API product search success' }));
      let allowedData = [];
      if (Array.isArray(result.data) && result.data.length > 0) {
        result.data.forEach(elm => {
          const parentAccountCds = elm.parent_account_cd ? elm.parent_account_cd.replace(/ /g, '').split(',') : [];
          const prodDisplay = isValid(elm.start_display, elm.end_display);
          if (prodDisplay && parentAccountCds.includes(user.parent_account_cd) && elm.brand === brand) {
            allowedData.push(elm);
          }
        });
      } else {
        allowedData = [];
      }
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Response success', data: allowedData }));
      return allowedData;
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Call API product search', message: 'Error calling API product search', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
      return null;
    });
  },
  getProductCatalogCMS: (req, apiName) => {
    const { user } = req;
    const queryString = req.query;
    const keys = Object.keys(queryString);
    const querys = [];
    for (let i = 0; i < keys.length; i++) {
      const key = keys[i];
      if (!(key === 'parent-account-cd' || key === 'account-cd' || key === 'dealer-id')) {
        querys.push([key, queryString[key]]);
      }
    }
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    querys.push(['parent-account-cd', user.parent_account_cd]);
    querys.push(['account-cd', user.account_cd]);
    querys.push(['dealer-id', user.dealer_id]);
    const fallbackOptions = {};
    const requestOptions = jClone.clone(config.apis[apiName]);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user && user.msisdn ? user.msisdn : null;

    return hystrixutil.request(apiName, requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Call API ' + apiName, message: 'Call API ' + apiName + ' success' }));
      if (Array.isArray(result.data)) {
        let allowedData = [];
        if (result.data.length > 0 && result.data[0].start_display && result.data[0].end_display) {
          result.data.forEach(elm => {
            const prodDisplay = isValid(elm.start_display, elm.end_display);
            const parentAccountCds = elm.parent_account_cd ? elm.parent_account_cd.replace(/ /g, '').split(',') : [user.parent_account_cd];
            const sanityDisplay = isValid(elm.sanity_start_display, elm.sanity_end_display);
            const sanityParentAccountCds = elm.sanity_parent_account_cd ? elm.sanity_parent_account_cd.replace(/ /g, '').split(',') : [];
            const whitelistType = elm.whitelisted_type;
            const filter = whitelistType ? require('../config/filters/' + whitelistType) : null;
            const filterParam = elm.whitelisted_param;
            const isFiltered = filter && filterParam ? filter.whitelist.includes(getRequestParamValueByString(req, filterParam)) && !filter.blacklist.includes(getRequestParamValueByString(req, filterParam)) : true;
            if (((prodDisplay && parentAccountCds.includes(user.parent_account_cd)) || (sanityDisplay && sanityParentAccountCds.includes(user.parent_account_cd))) && isFiltered) {
              //delete elm.start_display;
              //delete elm.end_display;
              delete elm.sanity_start_display;
              delete elm.sanity_end_display;
              delete elm.parent_account_cd;
              delete elm.sanity_parent_account_cd;
              delete elm.whitelisted_type;
              delete elm.whitelisted_param;
              allowedData.push(elm);
              markMCCMPersonalizedBannerAsRead(req, elm, querys);
            }
          });
        } else {
          result.data.forEach(elm => {
            delete elm.start_display;
            delete elm.end_display;
            delete elm.sanity_start_display;
            delete elm.sanity_end_display;
            delete elm.parent_account_cd;
            delete elm.sanity_parent_account_cd;
            delete elm.whitelisted_type;
            delete elm.whitelisted_param;
          });
          allowedData = result.data;
        }
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Response success', data: allowedData }));
        return allowedData;
      } else {
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Response failed' }));
        return null;
      }
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Call API ' + apiName, message: 'Error call API ' + apiName, error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
      return null;
    });
  },
  doGetXboxIds: (req) => {
    return module.exports.getSubsInfo(req).then(subsInfo => {
      if (subsInfo && subsInfo.SUBSCRIBER_NUMBER && subsInfo.PRICEPLAN && subsInfo.PAYMENT_CATEGORY) {
        const subsNo = subsInfo.SUBSCRIBER_NUMBER;
        const pricePlan = subsInfo.PRICEPLAN;
        const paymentCat = subsInfo.PAYMENT_CATEGORY;
        const promises = [];
        promises.push(module.exports.getXboxIds(req, subsNo, pricePlan, paymentCat));
        return Promise.all(promises).then(results => {
          const xboxIds = results[0] ? results[0] : [];
          return xboxIds;
        });
      } else {
        return [];
      }
    });
  },
  getXboxIds: (req, subsNo, pricePlan, paymentCat) => {
    const { user } = req;
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const xboxIdsTemp = [];
    const querys = [];
    querys.push(['subsNo', subsNo]);
    querys.push(['pricePlan', pricePlan]);
    querys.push(['paymentCat', paymentCat + '-PAID']);

    const requestOptions = jClone.clone(config.apis['xbox']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;

    return hystrixutil.request('xbox', requestOptions, apigatehttp.request, fallbackOptions, fallback, querys).then(result => {
      if (result && result.data && result.data.XBOXResponse && result.data.XBOXResponse.Menu_ID) {
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Calling XBOX API', message: 'Calling XBOX API success' }));
        const xboxIdString = result.data.XBOXResponse.Menu_ID;
        const xboxIdsList = xboxIdString.split(",");
        for (let i = 0; i < xboxIdsList.length; i++) {
          if (isNaN(xboxIdsList[i]) && xboxIdsTemp.indexOf(xboxIdsList[i]) < 0) {
            xboxIdsTemp.push(xboxIdsList[i]);
          }
        }
        return xboxIdsTemp;
      } else {
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Calling XBOX API', message: 'Calling XBOX API failed' }));
        return [];
      }
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Calling XBOX API', message: 'Error calling XBOX API', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
      return [];
    });
  },
  getSubsInfo: (req) => {
    const { user } = req;
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const querys = [];
    querys.push(['ax-request-id', uuidv1()]);
    querys.push(['msisdn', user.msisdn]);

    const requestOptions = jClone.clone(config.apis['getsubscriberinfo']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;

    return hystrixutil.request('getsubscriberinfo', requestOptions, apigatehttp.request, fallbackOptions, fallback, querys).then(result => {
      if (result && result.data && result.data.data && result.data.code === '00') {
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Calling API Get Subs Info', message: 'Calling API Get Subs Info success' }));
        const attributes = result.data.data;
        const subsInfo = attributes.reduce(function (accumulator, currentValue) {
          accumulator[currentValue.name] = currentValue.value;
          return accumulator;
        }, {})
        return subsInfo
      } else {
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Calling API Get Subs Info', message: 'Calling API Get Subs Info failed' }));
        return [];
      }
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Calling API Get Subs Info', message: 'Error calling API Get Subs Info', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
      return [];
    });
  }
}

function isValid(startDate, endDate) {
  let current = (new Date()).toISOString();
  current = current.substring(0, 19).replace(/[-:T]/g, '');
  const start = startDate ? startDate.substring(0, 19).replace(/[-:T]/g, '') : current;
  const end = endDate ? endDate.substring(0, 19).replace(/[-:T]/g, '') : current;
  if (current >= start && current <= end) {
    return true;
  } else {
    return false;
  }
}

function getRequestParamValueByString(req, paramPath) {
  paramPath = paramPath.startsWith('req.') ? paramPath.substring(4) : paramPath;
  const paramPaths = paramPath.split('.');
  let temp = {};
  for (let i = 0; i < paramPaths.length; i++) {
    if (i === 0) {
      temp = req[paramPaths[i]];
    } else {
      temp = temp[paramPaths[i]];
    }
  }
  return temp;
}

function markMCCMPersonalizedBannerAsRead(req, elm, querys) {
  const user = req.user ? req.user : {};
  let isOfferIdExist = false;
  for (let i = 0; i < querys.length; i++) {
    if (querys[i].includes('offer-id')) {
      isOfferIdExist = true;
      break;
    }
  }
  if (isOfferIdExist && elm.mccm_banner && elm.mccm_banner === 'yes') {
    const fallbackOptions = {};
    const requestOptions = jClone.clone(config.apis['mccmmarkasread']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;
    querys.push(['msisdn', user.msisdn]);
    hystrixutil.request('mccmmarkasread', requestOptions, apigatehttp.request, fallbackOptions, fallback, querys).then(response => {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Call API MCCM callback', message: 'Calling API MCCM callback success' }));
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Call API MCCM callback', message: 'Error calling API MCCM callback', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
    });
  }
}