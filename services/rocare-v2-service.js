/**
 * @name ro-care-v2-service
 * @description This module serve ro care
 */
'use strict';

const hydraExpress = require('hydra-express');
const stringify = require('jsesc');
const hystrixutil = require('@xl/hystrix-util');
const config = require('fwsp-config').getObject();
const axios = require('axios');

const jClone = require('@xl/json-clone');
const rc = require('../constant/RC');

function doGetSearchFAQ(req, res) {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request Get Search FAQ' }));
    const keyword = req.query.keyword ? req.query.keyword : '';
    const lang = req.headers.language;
    const request = [];
    request.push(getRoCareFAQCategories(req));
    request.push(getRoCareFAQ(req));
    Promise.all(request).then(result => {
        const resultFAQCategories = [];
        const FAQCategories = result[0].data;
        const FAQ = result[1].data;
        FAQCategories.sort((a, b) => (a.position > b.position) ? 1 : ((b.position > a.position) ? -1 : 0));
        FAQCategories.map(res => {
            const FAQFilteredByCategory = FAQ.filter(resFAQ => resFAQ.faq_category == res.faq_category);
            FAQFilteredByCategory.sort((a, b) => (a.position > b.position) ? 1 : ((b.position > a.position) ? -1 : 0));
            const FAQFilteredByKeyword = FAQFilteredByCategory.filter(resFAQ => resFAQ.title.toLowerCase().indexOf(keyword.toLowerCase()) !== -1);
            if (FAQFilteredByKeyword && FAQFilteredByKeyword.length > 0) {
                FAQFilteredByKeyword.map(resFAQ => delete resFAQ.faq_category);
                const obj = {
                    title: res.faq_category,
                    position: res.position,
                    faqs: FAQFilteredByKeyword,
                }
                resultFAQCategories.push(obj);
            }
        });

        const FAQCategoriesFilteredByKeyword = FAQCategories.filter(res => res.faq_category.toLowerCase().indexOf(keyword.toLowerCase()) !== -1);
        if (FAQCategoriesFilteredByKeyword.length > 0) {
            FAQCategoriesFilteredByKeyword.map(resCategory => {
                const isCategoryExist = resultFAQCategories.filter(res => res.title.toLowerCase().indexOf(resCategory.faq_category.toLowerCase()) !== -1).length > 0;
                if (!isCategoryExist) {
                    const FAQFiltered = FAQ.filter(resFAQ => resFAQ.faq_category == resCategory.faq_category);
                    FAQFiltered.sort((a, b) => (a.position > b.position) ? 1 : ((b.position > a.position) ? -1 : 0));
                    if (FAQFiltered && FAQFiltered.length > 0) {
                        FAQFiltered.map(resFAQ => delete resFAQ.faq_category);
                        const obj = {
                            title: resCategory.faq_category,
                            position: resCategory.position,
                            faqs: FAQFiltered,
                        }
                        resultFAQCategories.push(obj);
                    }
                }
            });
        }
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request Get FAQ', message: 'Request Get FAQ is exist' }));
        res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: resultFAQCategories });
    }).catch(error => {
        hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Retrieving FAQ', message: 'Error doing Retrieving FAQ', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
        res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
    });
}

function getRoCareFAQCategories(req) {
    const { user } = req;
    const fallbackOptions = {};
    const fallback = (error, args) => {
        return Promise.reject({
            message: error.message,
            args
        });
    };
    const lang = req.headers.language;
    const authHeader = req.headers.authorization;
    const requestOptions = jClone.clone(config.apis['selfrocarefaqcategories']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;
    const querys = [];
    querys.push(['authHeader', authHeader]);
    querys.push(['language', lang]);
    return hystrixutil.request('selfrocarefaqcategories', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request Get RO Care FAQ Categories', message: 'Request Get RO Care FAQ Categories success' }));
        return { status: true, data: result.data.result.data };
    }).catch(error => {
        hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Request Get RO Care FAQ Categories', message: 'Request Get FAQ Categories failed', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
        return { status: false, data: [] };
    });
}

function getRoCareFAQ(req) {
    const { user } = req;
    const fallbackOptions = {};
    const fallback = (error, args) => {
        return Promise.reject({
            message: error.message,
            args
        });
    };
    const lang = req.headers.language;
    const authHeader = req.headers.authorization;
    const requestOptions = jClone.clone(config.apis['selfrocarefaq']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;
    const querys = [];
    querys.push(['authHeader', authHeader]);
    querys.push(['language', lang]);
    return hystrixutil.request('selfrocarefaq', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request Get RO Care FAQ', message: 'Request Get RO Care FAQ success' }));
        return { status: true, data: result.data.result.data };
    }).catch(error => {
        hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Request Get RO Care FAQ', message: 'Request Get RO Care FAQ failed', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
        return { status: false, data: [] };
    });
}

function searchFAQ(data, keyword) {
    data.sort((a, b) => (a.position > b.position) ? 1 : ((b.position > a.position) ? -1 : 0));
    const filtered = data.filter(res => {
        return res.title.toLowerCase().indexOf(keyword.toLowerCase()) !== -1;
    });
    return { code: (filtered.length > 0) ? '00' : '41', data: filtered };
}

function doGetLoginFAQ(req, res) {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request Get Login FAQ' }));
    const fallbackOptions = {};
    const fallback = (error, args) => {
        return Promise.reject({
            message: error.message,
            args
        });
    };
    const lang = req.headers.language;
    const authHeader = req.headers.authorization;
    const keyword = (typeof req.query['keyword'] !== 'undefined' && req.query['keyword']) ? req.query['keyword'] : "";
    hydraExpress.appLogger.info(stringify({ keyword: keyword }));
    const requestOptions = jClone.clone(config.apis['selffaqlogin']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = '';
    const querys = [];
    querys.push(['authHeader', authHeader]);
    querys.push(['language', lang]);
    hystrixutil.request('selffaqlogin', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
        const resultFAQ = result.data.result.data;
        const dataSearchFAQ = searchFAQ(resultFAQ, keyword);
        if(dataSearchFAQ.code == '00'){
            hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request Get FAQ', message: 'Request Get FAQ is exist' }));
            res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: dataSearchFAQ.data });
        }else{
            hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request Get FAQ', message: 'Request Get FAQ is not exist' }));
            res.sendOk({ errorCode: rc.codes['41'].rc, errorMessage: rc.i18n('41', lang).rm });
        }
    }).catch(error => {
        hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Retrieving FAQ', message: 'Error doing Retrieving FAQ', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
        res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
    });
}

function doGetFaqMiniRO(req, res) {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request Get Search FAQ (Mini RO)' }));
    const { user } = req;
    const keyword = req.query.keyword ? req.query.keyword : '';
    const fallbackOptions = {};
    const fallback = (error, args) => {
        return Promise.reject({
            message: error.message,
            args
        });
    };
    const lang = req.headers.language;
    const authHeader = req.headers.authorization;
    const requestOptions = jClone.clone(config.apis['selfrocareminirofaq']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;
    const querys = [];
    querys.push(['authHeader', authHeader]);
    querys.push(['language', lang]);
    hystrixutil.request('selfrocareminirofaq', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
        const resultFAQ = result.data.result.data;
        const dataSearchFAQ = searchFAQ(resultFAQ, keyword);
        if(dataSearchFAQ.code == '00'){
            hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request Get FAQ', message: 'Request Get FAQ is exist' }));
            res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: dataSearchFAQ.data });
        }else{
            hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request Get FAQ', message: 'Request Get FAQ is not exist' }));
            res.sendOk({ errorCode: rc.codes['41'].rc, errorMessage: rc.i18n('41', lang).rm });
        }
    }).catch(error => {
        hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Retrieving FAQ', message: 'Error doing Retrieving FAQ', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
        res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
    });
}

module.exports = {
    getSearchFAQ: doGetSearchFAQ,
    getLoginFAQ: doGetLoginFAQ,
    getFaqMiniRO: doGetFaqMiniRO,
}
