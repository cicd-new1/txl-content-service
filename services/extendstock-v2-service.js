/**
 * @name extendstock-v2-service
 * @description This module serve extend stock
 */
'use strict';

const hydraExpress = require('hydra-express');
const stringify = require('jsesc');
const hystrixutil = require('@xl/hystrix-util');
const config = require('fwsp-config').getObject();
const axios = require('axios');

const jClone = require('@xl/json-clone');
const rc = require('../constant/RC');

function doGetExtendStock(req, res) {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request Get Extend Stock' }));
    const lang = req.headers.language;
    const requests = [];
    const { apiName } = req.params;
    if (apiName === 'list' || apiName === 'detail') {
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request for Extend Stock ' + apiName + ' API' }));
        const cmsAPI = apiName === 'list' ? 'extendstocklist' : 'extendstockdetail';
        requests.push(getExtendStockCMS(req, cmsAPI));
        Promise.all(requests).then(results => {
            const cmsProgram = results[0] ? results[0] : [];
            res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: cmsProgram });
        }).catch(error => {
            hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Call API combined news homepage', message: 'Error calling API combined news homepage', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
            hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
            res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
        });
    } else {
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Program API Not Found' }));
        res.sendNotFound({ errorCode: rc.codes['12'].rc, errorMessage: rc.i18n('12', lang).rm });
    }
}

function getExtendStockCMS(req, apiName) {
    const { user } = req;
    const querys = [];
    const { brand, productCode } = req.query;

    const fallback = (error, args) => {
        return Promise.reject({
            message: error.message,
            args
        });
    };
    querys.push(['brand', brand]);
    querys.push(['product-code', productCode]);

    const fallbackOptions = {};
    const requestOptions = jClone.clone(config.apis[apiName]);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user && user.msisdn ? user.msisdn : null;

    return hystrixutil.request(apiName, requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Call API ' + apiName, message: 'Call API ' + apiName + ' success' }));
        if (Array.isArray(result.data)) {
            hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Response success', data: result.data }));
            return result.data;
        } else {
            hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Response failed' }));
            return null;
        }
    }).catch(error => {
        hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Call API ' + apiName, message: 'Error call API ' + apiName, error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
        return null;
    });
}

module.exports = {
    getExtendStock: doGetExtendStock,
    cacheOptions: {
        appendKey: (req, res) => {
            return req.user && req.user.msisdn ? req.user.msisdn : req.client.client_id;
        },
        headers: {
            'cache-control': 'no-cache, no-store, must-revalidate',
            'pragma': 'no-cache',
            'expires': 0
        }
    },
    getMsisdnCacheToggle: () => {
        return function (req, res) {
            return req.user && req.user.msisdn;
        }
    }
}