/**
 * @name content-v2-service
 * @description This module serve content v2 route.
 */
'use strict';

const config = require('fwsp-config');
const aes256 = require('@xl/custom-aes256');
const indirectObject = require('@xl/indirect-object-handler');

const beCipher = new aes256();
beCipher.createCipher(config.pin.backend);

module.exports = {
  encryptedObjectHandler: (req, res, next) => {
    const authHeader = req.headers.authorization;
    const { user } = req;
    if(user) {
      user.parent_account_cd = indirectObject.decrypt(beCipher, user.parent_account_cd, authHeader);
      user.account_cd = indirectObject.decrypt(beCipher, user.account_cd, authHeader);
      user.dealer_id = indirectObject.decrypt(beCipher, user.dealer_id, authHeader);
    }
    if(req.query['parent-account-cd']) {
      const tempParentAccountCd = req.query['parent-account-cd'];
      req.query['parent-account-cd'] = indirectObject.decrypt(beCipher, req.query['parent-account-cd'], authHeader);
      const oriUrl = req.originalClearUrl || req.originalUrl || req.url;
      req.originalClearUrl = oriUrl.replace(encodeURIComponent(tempParentAccountCd), req.query['parent-account-cd']);
    }
    if(req.query['account-cd']) {
      const tempAccountCd = req.query['account-cd'];
      req.query['account-cd'] = indirectObject.decrypt(beCipher, req.query['account-cd'], authHeader);
      const oriUrl = req.originalClearUrl || req.originalUrl || req.url;
      req.originalClearUrl = oriUrl.replace(encodeURIComponent(tempAccountCd), req.query['account-cd']);
    }
    if(req.query['dealer-id']) {
      const tempDealerId = req.query['dealer-id'];
      req.query['dealer-id'] = indirectObject.decrypt(beCipher, req.query['dealer-id'], authHeader);
      const oriUrl = req.originalClearUrl || req.originalUrl || req.url;
      req.originalClearUrl = oriUrl.replace(encodeURIComponent(tempDealerId), req.query['dealer-id']);
    }
    next();
  },
  decryptedObjectHandler: (req, res, next) => {
    const authHeader = req.headers.authorization;
    const { user } = req;
    if (req.query['product-code'] && req.query['flag-encrypt'] === 'true') {
      const tempProductCode = req.query['product-code'];
      const decProductCode = indirectObject.decrypt(beCipher, tempProductCode, authHeader);
      const splits = decProductCode.split('|');
      req.query['product-code'] = splits.length > 0 ? splits[1] : '';
    }
    next();
  }
}