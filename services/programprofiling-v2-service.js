/**
 * @name programprofiling-v2-service
 * @description This module serve content v2 route program profiling.
 */
 'use strict';

 const hydraExpress = require('hydra-express');
 const hystrixutil = require('@xl/hystrix-util');
 const masheryhttp = require('@xl/mashery-http');
 const authenticator = require('@xl/txl-remote-authenticate');
 const config = require('fwsp-config');
 const cfenv = require('cfenv');
 const axios = require('axios');
 const stringify = require('jsesc');
 const jClone = require('@xl/json-clone');
 const aes256 = require('@xl/custom-aes256');
 const intercomm = require('@xl/cf-intercomm');
 const indirectObject = require('@xl/indirect-object-handler');
 const rc = require('../constant/RC');
 const uuidv1 = require('uuid/v1');

 const productProfilingService = require('./productprofiling-v2-service');

 const beCipher = new aes256();
 beCipher.createCipher(config.pin.backend);

 module.exports = {
  getMsisdnCacheToggle: () => {
    return function(req, res) {
      return req.user && req.user.msisdn;
    }
  },
  msisdnCacheOptions : {
    appendKey: (req, res) => {
      return req.user && req.user.msisdn ? req.user.msisdn : req.client.client_id;
    },
    headers: {
      'cache-control': 'no-cache, no-store, must-revalidate',
      'pragma': 'no-cache',
      'expires': 0
    }
  },
  cacheOptions: {
    appendKey: (req, res) => {
      return req.user && req.user.msisdn ? req.user.msisdn : req.client.client_id;
    },
    headers: {
      'cache-control': 'no-cache, no-store, must-revalidate',
      'pragma': 'no-cache',
      'expires': 0
    }
  },
  getCacheDurationInMillis: () => {
    const today = new Date();
    const seconds = today.getSeconds();
    const minutesToSeconds = today.getMinutes() * 60;
    const hoursToSeconds = today.getHours() * 60 * 60;
    const result = hoursToSeconds + minutesToSeconds + seconds;
    return (86400 - result) * 1000;
  },
  getRoProgram: (req, res) => {
    /*
    req.user = {
      msisdn: '6287787237069',
      parent_account_cd: '90057197'
    }
    */
    const lang = req.headers.language;
    const requests = [];
    const { apiName } = req.params;
    if (apiName === 'list' || apiName === 'detail') {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Request for RO Program '+apiName+' API'}));
      const cmsAPI = apiName === 'list' ? 'roprogramlist' : 'roprogramdetail';
      requests.push(module.exports.getProfilingProgramRule(req));
      requests.push(productProfilingService.getProductCMS(req, cmsAPI));
      Promise.all(requests).then(results => {
        const profilingRule = results[0] ? results[0] : [] ;
        const cmsProgram = results[1] ? results[1] : [];
        const eligibleProgram = [];
        let product;
        if (cmsProgram && cmsProgram.length > 0) {
          cmsProgram.forEach(elm => {
            elm.location = 'DEFAULT';
            if (!elm.city_level) {
              hydraExpress.appLogger.info(stringify({id: req.logId, event: 'List product', message: 'RO program profiling city empty'}));
              elm.banner_appear = '';
              eligibleProgram.push(elm);
            } else{
              const localBenefit = [];
              const localValue = elm.city_level.replace(/ /g, '').split('$');
              for(let i = 0; i < localValue.length; i++) {
                const item = localValue[i].split('|');
                localBenefit.push(item);
              }
              hydraExpress.appLogger.info(stringify({id: req.logId, event: 'City level ro program list', message: 'City level RO program list', data:localBenefit}));

              //filter banner_id on profiling rule level
              const matchedProgramProfile = profilingRule.filter(i => i.program_id.includes(elm.news_id));
              hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Local Benefit match profiling rule', message: 'Local Benefit match profiling rule', data:matchedProgramProfile}));

              const matchedLocalBenefit = [];
              for (let i=0; i < localBenefit.length; i++){
                for (let j=0; j< matchedProgramProfile.length; j++){
                  if(localBenefit[i].indexOf(matchedProgramProfile[j].level) >= 0) {
                    matchedLocalBenefit.push(localBenefit[i]);
                  }
                }
              }
              hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Local Benefit match profile detail', message: 'Local Benefit match profile detail', data:matchedLocalBenefit}));

             if (matchedProgramProfile && matchedProgramProfile.length > 0) {
              for (let i=0; i<matchedProgramProfile.length; i++){
                const programIdList = matchedProgramProfile[i].program_id.replace(/ /g, '').split(',') ;
                for (let j=0; j<programIdList.length; j++){
                  if (programIdList[j] === elm.news_id) {
                    elm.location = matchedProgramProfile[i].city;
                    elm.program_appear = 'Berlaku di '+ matchedProgramProfile[i].city;
                    eligibleProgram.push(elm);
                  }
                }
              }
             }
            }
          });
        }
        res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: eligibleProgram});

      }).catch(error => {
        hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call API combined news homepage', message: 'Error calling API combined news homepage', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
        res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
      });
    } else {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Program API Not Found'}));
      res.sendNotFound({errorCode: rc.codes['12'].rc, errorMessage: rc.i18n('12', lang).rm});
    }


  },
  getProfilingProgramRule: (req) => {
    const { user } = req;
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const authHeader = req.headers.authorization;
    const requestOptions = jClone.clone(config.apis['selfprofilingprogramrule']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;
    const querys = [];
    querys.push(['authHeader', authHeader]);
    return hystrixutil.request('selfprofilingprogramrule', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Call API profiling program rule', message: 'Calling API profiling program rule success', data: result.data}));
      const data = result && result.data && result.data.result  ? result.data.result : [];
      return data;
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call API profiling program rule', message: 'Error calling API profiling program rule', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
      return [];
    });
  },
  getCMSRoProgramList: (req) => {
    const { user } = req;
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const authHeader = req.headers.authorization;
    const requestOptions = jClone.clone(config.apis['selfroprogramlist']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;
    const querys = [];
    querys.push(['authHeader', authHeader]);
    return hystrixutil.request('selfroprogramlist', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Call API CMS RO program list', message: 'Calling API CMS RO program list success', data: result.data}));
      const data = result && result.data && result.data.result && result.data.result.data  ? result.data.result.data : [];
      return data;
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call API CMS RO program list', message: 'Error calling API CMS RO program list', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
      return [];
    });
  }
 }