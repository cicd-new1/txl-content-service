/**
 * @name bannerprofiling-v2-service
 * @description This module serve content v2 route.
 */
 'use strict';

 const hydraExpress = require('hydra-express');
 const hystrixutil = require('@xl/hystrix-util');
 const masheryhttp = require('@xl/mashery-http');
 const config = require('fwsp-config');
 const axios = require('axios');
 const stringify = require('jsesc');
 const jClone = require('@xl/json-clone');
 const aes256 = require('@xl/custom-aes256');
 const rc = require('../constant/RC');

 const personalHomepageService = require('./personalhomepage-v2-service');

 const beCipher = new aes256();
 beCipher.createCipher(config.pin.backend);

 module.exports = {
  getNewsDetail: (req, res) => {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Request for news detail homepage profiling API'}));
    /*
    req.user = {
      msisdn: '6287787237069',
      parent_account_cd: '90057197'
    }
    */

    const lang = req.headers.language;
    const requests = [];
    requests.push(module.exports.getSelfNewsDetail(req));
    requests.push(module.exports.getProfilingBannerRule(req));
    Promise.all(requests).then(results => {
      const newsDetail = results[0] ? results[0] : [];
      const profilingRule = results[1] ? results[1] : [];
      const eligibleDetail = [];
      if (newsDetail && newsDetail.length > 0) {
        newsDetail.forEach(elm => {
          elm.location = 'DEFAULT';
          if (!elm.city_level) {
            hydraExpress.appLogger.info(stringify({id: req.logId, event: 'List product', message: 'RO banner profiling city empty'}));
            elm.banner_appear = '';
            eligibleDetail.push(elm);
          } else{
            const localBenefit = [];
            const localValue = elm.city_level.replace(/ /g, '').split('$');
            for(let i = 0; i < localValue.length; i++) {
              const item = localValue[i].split('|');
              localBenefit.push(item);
            }
            hydraExpress.appLogger.info(stringify({id: req.logId, event: 'City level ro banner list', message: 'City level RO banner list', data:localBenefit}));

            //filter banner_id on profiling rule level
            const matchedProgramProfile = profilingRule.filter(i => i.banner_id.includes(elm.news_id));
            hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Local Benefit match profiling rule', message: 'Local Benefit match profiling rule', data:matchedProgramProfile}));

            const matchedLocalBenefit = [];
            for (let i=0; i < localBenefit.length; i++){
              for (let j=0; j< matchedProgramProfile.length; j++){
                if(localBenefit[i].indexOf(matchedProgramProfile[j].level) >= 0) {
                  matchedLocalBenefit.push(localBenefit[i]);
                }
              }
            }
            hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Local Benefit match profile detail', message: 'Local Benefit match profile detail', data:matchedLocalBenefit}));

           if (matchedProgramProfile && matchedProgramProfile.length > 0) {
            for (let i=0; i<matchedProgramProfile.length; i++){
              const programIdList = matchedProgramProfile[i].banner_id.replace(/ /g, '').split(',') ;
              for (let j=0; j<programIdList.length; j++){
                if (programIdList[j] === elm.news_id) {
                  elm.location = matchedProgramProfile[i].city;
                  elm.program_appear = 'Berlaku di '+ matchedProgramProfile[i].city;
                  eligibleDetail.push(elm);
                }
              }
            }
           }
          }
        });
      }
      res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: eligibleDetail});
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call API combined news detail and profiling banner', message: 'Error calling API combined news detail and profiling banner', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
      res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
    });
  },

  getPersonalHomepage: (req, res) => {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Request for personal homepage API'}));
    /*
    req.user = {
      msisdn: '6287787237069',
      parent_account_cd: '90057197'
    }
    */

    const lang = req.headers.language;
    let maxBanner = req.query.maxbanner ? req.query.maxbanner : 6;
    const data = [];
    const requests = [];
    requests.push(personalHomepageService.getCMSPersonalHomepage(req, res));
    requests.push(personalHomepageService.getMCCMPersonalHomepage(req, res));
    requests.push(module.exports.getProfilingBannerRule(req));
    Promise.all(requests).then(results => {
      const cmsBanners = results[0] ? results[0] : [];
      const mccmBanners = results[1] ? results[1] : [];
      const profilingRule = results[2] ? results[2] : [];
      const eligibleBanner = [];
      let product;
      if (cmsBanners && cmsBanners.length > 0) {
        cmsBanners.forEach(elm => {
          elm.location = 'DEFAULT';
          if (!elm.city_level) {
            hydraExpress.appLogger.info(stringify({id: req.logId, event: 'List product', message: 'Banner profiling city empty'}));
            elm.banner_appear = '';
            eligibleBanner.push(elm);
          } else{
            const localBenefit = [];
            const localValue = elm.city_level.replace(/ /g, '').split('$');
            for(let i = 0; i < localValue.length; i++) {
              const item = localValue[i].split('|');
              localBenefit.push(item);
            }
            hydraExpress.appLogger.info(stringify({id: req.logId, event: 'City level banner homepage list', message: 'City level banner homepage list', data:localBenefit}));

            //filter banner_id on profiling rule level
            const matchedBannerProfile = profilingRule.filter(i => i.banner_id.includes(elm.news_id));
            hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Local Benefit match profiling rule', message: 'Local Benefit match profiling rule', data:matchedBannerProfile}));

            const matchedLocalBenefit = [];
            for (let i=0; i < localBenefit.length; i++){
              for (let j=0; j< matchedBannerProfile.length; j++){
                if(localBenefit[i].indexOf(matchedBannerProfile[j].level) >= 0) {
                  matchedLocalBenefit.push(localBenefit[i]);
                }
              }
            }
            hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Local Benefit match profile detail', message: 'Local Benefit match profile detail', data:matchedLocalBenefit}));

           if (matchedBannerProfile && matchedBannerProfile.length > 0) {
            for (let i=0; i<matchedBannerProfile.length; i++){
              const bannerIdList = matchedBannerProfile[i].banner_id.replace(/ /g, '').split(',') ;
              for (let j=0; j<bannerIdList.length; j++){
                if (bannerIdList[j] === elm.news_id) {
                  elm.location = matchedBannerProfile[i].city;
                  elm.banner_appear = 'Berlaku di '+ matchedBannerProfile[i].city;
                  eligibleBanner.push(elm);
                }
              }
            }
           }

          }
        });
        const seenNames = {};
        product = eligibleBanner.filter(function (currentObject) {
          if (currentObject.news_id in seenNames) {
            return false;
          } else {
            seenNames[currentObject.news_id] = true;
            return true;
          }
        });
      }

      const temp = [...eligibleBanner, ...mccmBanners];
      temp.sort((a,b) => (a.banner_position > b.banner_position) ? 1 : ((b.banner_position > a.banner_position) ? -1 : 0));
      maxBanner = maxBanner > temp.length ? temp.length : maxBanner;
      for(let i = 0; i < maxBanner; i++) {
        data.push(temp[i]);
      }
      res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data});
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call API combined news homepage', message: 'Error calling API combined news homepage', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
      res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
    });
  },
  getProfilingBannerRule: (req) => {
    const { user } = req;
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const authHeader = req.headers.authorization;
    const requestOptions = jClone.clone(config.apis['selfprofilingbannerrule']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;
    const querys = [];
    querys.push(['authHeader', authHeader]);
    return hystrixutil.request('selfprofilingbannerrule', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Call API profiling banner rule', message: 'Calling API profiling banner rule success', data: result.data}));
      const data = result && result.data && result.data.result  ? result.data.result : [];
      return data;
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call API profiling banner rule', message: 'Error calling API profiling banner rule', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
      return [];
    });
  },
  getSelfNewsDetail: (req) => {
    const { user } = req;
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const authHeader = req.headers.authorization;
    const requestOptions = jClone.clone(config.apis['selfnewsdetail']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;
    const querys = [];
    querys.push(['authHeader', authHeader]);
    querys.push(['news-id', req.query['news-id']]);
    return hystrixutil.request('selfnewsdetail', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Call API self news detail', message: 'Calling API self news detail success', data: result.data}));
      const data = result && result.data && result.data.result && result.data.result.data  ? result.data.result.data : [];
      return data;
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call API self news detail', message: 'Error calling API self news detail', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
      return [];
    });
  }
 }