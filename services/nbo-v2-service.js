/**
 * @name nbo-v2-service
 * @description This module serve NBO Service
 */
'use strict';

const hydraExpress = require('hydra-express');
const stringify = require('jsesc');
const hystrixutil = require('@xl/hystrix-util');
const config = require('fwsp-config').getObject();
const axios = require('axios');
const masheryhttp = require('@xl/mashery-http');
const uuidv1 = require('uuid/v4');
const jClone = require('@xl/json-clone');
const rc = require('../constant/RC');
const apigatehttp = require('@xl/apigate-http');
const indirectObject = require('@xl/indirect-object-handler');
const AES256 = require('@xl/custom-aes256');
const beCipher = new AES256();
beCipher.createCipher(config.pin.backend);

function doGetNboPackage(req, res) {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request NBO Package' }));
    const lang = req.headers.language;
    const { msisdn } = req.params;

    getPrefix(req, msisdn).then(result => {
        if (result.errorCode === '00') {
            let channel;
            const brand = result.data;
            if (result && result.data === 'XL') {
                channel = 'SIDOMPUL_XLPRE';
            } else if (result && result.data === 'AXIS') {
                channel = 'SIDOMPUL_AXIS';
            }
            getNBO(req, channel, brand).then(nbos => {
                if (nbos && nbos.length > 0) {
                    // takeout offer when value is NA
                    let i = nbos.length;
                    while (i--) {
                        if (nbos[i].product_name === 'NA' || nbos[i].dompul_price === 'NA' || nbos[i].product_name_mccm === 'NA') {
                            nbos.splice(i, 1);
                        }
                    }
                    if (nbos && nbos.length > 0) {
                        sendNotification(req, res, 0, msisdn);
                        nbos.forEach(elm => {
                            elm.product_name_mccm = encryptProductCode(req, elm.product_name_mccm, elm.product_name);
                        });
                        res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: nbos });
                    } else {
                        res.sendNotFound({ errorCode: rc.codes['15'].rc, errorMessage: rc.i18n('15', lang).rm });
                    }
                } else {
                    res.sendNotFound({ errorCode: rc.codes['15'].rc, errorMessage: rc.i18n('15', lang).rm });
                }
            }).catch(error => {
                hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Check Get NBO', message: 'Error Get NBO', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
                res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
            });
        } else {
            hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
            res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
        }
    }).catch(error => {
        hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Check Get Prefix', message: 'Error Get Prefix', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
        res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
    });
}

function getNBO(req, channel, brand) {
    return doGetNBO(req, channel, brand).then(nbos => {
        const data = [...nbos];
        if (data.length > 0) {
            const promises = [];
            data.forEach(elm => {
                promises.push(getDetail(req, elm));
            });
            return Promise.all(promises).then(nboDetails => {
                return nboDetails.reduceRight(
                    (accumulator, currentValue) => accumulator.concat(currentValue)
                );
            });
        }
    });
}

function doGetNBO(req, channel, brand) {
    const { user } = req;
    const { msisdn, location, maxOffer } = req.params;
    const authHeader = req.headers.authorization;
    const fallbackOptions = {};
    const requestOptions = jClone.clone(config.apis['getnbov2']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = authHeader.startsWith('Bearer ') ? user.msisdn : '';
    const querys = [];
    querys.push(['msisdn', msisdn]);
    querys.push(['channel', channel]);
    querys.push(['location', location]);
    querys.push(['ax-request-id', uuidv1()]);

    // const data = '{"data": {"subscriberId":590354082,"msisdn":"6287885148024","offers":{"offer":[{"id":143296,"location":["NBO","Slot1"],"offer_tag":"NBO-SIDOMPUL-XL","channelAttributes":{"channelAttribute":[{"name":"DOMPUL_PRICE","value":"150988"},{"name":"OFFER_DETAIL_TEXT","value":"Anda memilih XTRA Combo 35GB+70GB, 30hr, Disc 30%, Rp167rb"},{"name":"SLOT_TEXT","value":"XC 35GB+70GB, 30hr, 167rb (Normal: 239rb - Disc 30%)"},{"name":"DOMPUL_BANNER_ID","value":"DEFAULT VALUE"},{"name":"CD_MAIN_ID_CHN","value":"TK_CLM_143296"},{"name":"DOMPUL_PKG_TOC","value":"Some Terms and Conditions about Some package"},{"name":"DOMPUL_PKG_DESCRIPTION","value":"Some Description about some package"},{"name":"PRODUCT_CODE","value":"XC3239"}]},"offerAttributes":{"offerAttribute":[{"name":"ACTIVE_PERIOD","value":"30"},{"name":"AMOUNT_0","value":"167000"},{"name":"BENEFIT_PRICE","value":"167000"},{"name":"BENEFIT_PRICE_ATTR_NAME","value":"AMOUNT_0"},{"name":"CD_MAIN_ID","value":"CLM_143296"},{"name":"MIN_BALANCE_0","value":"0"},{"name":"OFFER_TYPE","value":"OFFER"},{"name":"OfferType","value":"SUBSCRIPTION"},{"name":"SERVICE_ID","value":"8212787"},{"name":"SUBSCRIBER_MSISDN_ATTR_NAME","value":"MSIDSN"},{"name":"SUBSCRIPTION_TYPE","value":"VOICEDATASMSOT"},{"name":"TIER_COUNT","value":"1"},{"name":"OFFER_NAME","value":"XTRA Combo 35GB+70GB, 30hr, Rp167rb (Normal: Rp239rb - Disc 30%)"},{"name":"OFFER_DISCOUNT","value":"72000"},{"name":"BENEFIT_NAME","value":"XTRA Combo 35GB+70GB, 30hr, Rp239rb"},{"name":"BENEFIT_LIST_PRICE","value":"239000"},{"name":"BENEFIT_COST","value":"239000"},{"name":"BENEFIT_TYPE_NAME","value":"Subscription"}]}},{"id":179006,"location":["NBO","Slot3"],"offer_tag":"NBO-SIDOMPUL-XL","channelAttributes":{"channelAttribute":[{"name":"OFFER_DETAIL_TEXT","value":"Anda memilih XC Flex XXXL, 30hr, 125rb"},{"name":"DOMPUL_PRICE","value":"119770"},{"name":"DOMPUL_BANNER_ID","value":"DEFAULT VALUE"},{"name":"SLOT_TEXT","value":"XC Flex XXXL, 30hr, 125rb"},{"name":"PRODUCT_CODE","value":"FLEX125"},{"name":"DOMPUL_PKG_DESCRIPTION","value":"Some Description about some package"},{"name":"CD_MAIN_ID_CHN","value":"TK_CLM_179006"},{"name":"DOMPUL_PKG_TOC","value":"Some Terms and Conditions about Some package"}]},"offerAttributes":{"offerAttribute":[{"name":"ACTIVE_PERIOD","value":"30"},{"name":"AMOUNT_0","value":"125000"},{"name":"BENEFIT_PRICE","value":"125000"},{"name":"BENEFIT_PRICE_ATTR_NAME","value":"AMOUNT_0"},{"name":"CD_MAIN_ID","value":"CLM_179006"},{"name":"MIN_BALANCE_0","value":"0"},{"name":"OFFER_TYPE","value":"OFFER"},{"name":"OfferType","value":"SUBSCRIPTION"},{"name":"SERVICE_ID","value":"8213677"},{"name":"SUBSCRIBER_MSISDN_ATTR_NAME","value":"MSIDSN"},{"name":"SUBSCRIPTION_TYPE","value":"VOICEDATASMSOT"},{"name":"TIER_COUNT","value":"1"},{"name":"OFFER_NAME","value":"XC Flex XXXL, 30hr, 125rb"},{"name":"OFFER_DISCOUNT","value":"0"},{"name":"BENEFIT_NAME","value":"XC Flex XXXL, 30hr, 125rb"},{"name":"BENEFIT_LIST_PRICE","value":"125000"},{"name":"BENEFIT_COST","value":"125000"},{"name":"BENEFIT_TYPE_NAME","value":"Subscription"}]}}]}}}';
    // const result = Promise.resolve(JSON.parse(data));
    // return result.then(result => {
    return hystrixutil.request('getnbov2', requestOptions, apigatehttp.request, fallbackOptions, fallback, querys).then(result => {
        const offers = result && result.data && result.data.offers && result.data.offers.offer ? result.data.offers.offer : [];
        const response = [];
        if (offers) {
            if (Array.isArray(offers) && offers.length > 0) {
                for (let i = 0; i < offers.length; i++) {
                    const offer = offers[i];
                    const channelAttributes = offer.channelAttributes.channelAttribute;
                    const offerAttributes = offer.offerAttributes.offerAttribute;
                    const attributes = [channelAttributes, offerAttributes];
                    const exposedAttributes = ['SLOT_TEXT', 'DOMPUL_PRICE', 'BENEFIT_PRICE', 'PRODUCT_CODE'];
                    const object = {};
                    for (let i = 0; i < attributes.length; i++) {
                        for (let j = 0; j < attributes[i].length; j++) {
                            if (exposedAttributes.includes(attributes[i][j].name)) {
                                object[attributes[i][j].name] = attributes[i][j].value;
                            }
                        }
                    }
                    const nbo = {
                        brand,
                        product_name: object.SLOT_TEXT,
                        normal_price: object.BENEFIT_PRICE,
                        dompul_price: object.DOMPUL_PRICE,
                        product_name_mccm: object.PRODUCT_CODE,
                        offer_id: offer.id.toString(),
                    };
                    response.push(nbo);
                }
            }
            if (response.length > maxOffer) {
                response.splice(maxOffer, response.length - maxOffer)
            }
            // reverse sort to original from mccm
            response.reverse();
        }
        return response;
    }).catch(error => {
        hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Get NBO', message: 'Error Get NBO', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
        return [];
    });
    // });
}

function getPrefix(req, msisdn) {
    const fallbackOptions = {};
    const fallback = (error, args) => {
        return Promise.reject({
            message: error.message,
            args
        });
    };
    const bypassForce = req.headers['x-apicache-bypass'] ? req.headers['x-apicache-bypass'] : 'false';
    const { user } = req;
    const authHeader = req.headers.authorization;
    const requestOptions = jClone.clone(config.apis['selfcheckprefix']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = authHeader.startsWith('Bearer ') ? user.msisdn : '';
    const querys = [];
    querys.push(['msisdn', msisdn]);
    querys.push(['authHeader', authHeader]);
    querys.push(['bypassForce', bypassForce]);
    return hystrixutil.request('selfcheckprefix', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
        if (result.data.result.data && result.data.result.data.owner) {
            return { errorCode: '00', data: result.data.result.data.owner };
        } else {
            return { errorCode: '01', data: [] };
        }
    }).catch(error => {
        hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Check Prefix', message: 'Error Check Prefix', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
        return { errorCode: '01', data: [] };
    });
}

function doPostNboOfferShown(req, res) {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request NBO Offer Shown' }));
    const lang = req.headers.language;
    const { user } = req;
    const { msisdn, location, offerId } = req.params;
    const authHeader = req.headers.authorization;

    getPrefix(req, msisdn).then(result => {
        if (result.errorCode === '00') {
            let channel;
            if (result && result.data === 'XL') {
                channel = 'SIDOMPUL_XLPRE';
            } else if (result && result.data === 'AXIS') {
                channel = 'SIDOMPUL_AXIS';
            }
            const fallbackOptions = {};
            const requestOptions = jClone.clone(config.apis['postnbooffershownv2']);
            requestOptions.headers.event_id = req.logId;
            requestOptions.headers.actor = authHeader.startsWith('Bearer ') ? user.msisdn : '';
            const querys = [];
            querys.push(['msisdn', msisdn]);
            querys.push(['channel', channel]);
            querys.push(['location', location]);
            querys.push(['offerid', offerId]);
            querys.push(['ax-request-id', uuidv1()]);

            hystrixutil.request('postnbooffershownv2', requestOptions, apigatehttp.request, fallbackOptions, fallback, querys).then(result => {
                res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm });
            }).catch(error => {
                hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'NBO Offer Shown', message: 'Error NBO Offer Shown', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
                res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
            });
        } else {
            hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
            res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
        }
    }).catch(error => {
        hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Check Prefix', message: 'Error Check Prefix', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
        res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
    });
}

function fallback(error, args) {
    return Promise.reject({
        message: error.message,
        args
    });
}

function sendNotification(req, res, failedRequest, msisdn) {
    const fallbackOptions = {};
    const fallback = (error, args) => {
        return Promise.reject({
            message: error.message,
            args
        });
    };
    const { user } = req;
    const authHeader = req.headers.authorization;
    const requestOptions = jClone.clone(config.apis['selfsendsms']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = authHeader.startsWith('Bearer ') ? user.msisdn : '';
    const querys = [];
    querys.push(['authHeader', authHeader]);
    querys.push(['contactSource', 'NBOTOKOXL']);
    querys.push(['serviceId', 'PROMO']);
    querys.push(['contactType', 'GENERAL_POPUP_INFO']);
    querys.push(['msisdn', msisdn]);
    querys.push(['message', '']);
    return hystrixutil.request('selfsendsms', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
        if (result && result.data) {
            hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Sending SMS', message: 'Sending SMS success' }));
        } else {
            hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Sending SMS', message: 'Sending SMS failed' }));
            hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
            if (failedRequest < 3) {
                sendNotification(req, res, failedRequest += 1, msisdn);
            }
        }
    }).catch(error => {
        if (failedRequest < 3) {
            sendNotification(req, res, failedRequest += 1, msisdn);
        }
        hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Sending SMS', message: 'Error sending  SMS', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
    });
}

function getProductBenefitProfilling(req, productCode) {
    const fallbackOptions = {};
    const fallback = (error, args) => {
        return Promise.reject({
            message: error.message,
            args
        });
    };
    const { user } = req;
    const authHeader = req.headers.authorization;
    const requestOptions = jClone.clone(config.apis['selfproductbenefitprofiling']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = authHeader.startsWith('Bearer ') ? user.msisdn : '';
    const querys = [];
    querys.push(['authHeader', authHeader]);
    querys.push(['productCode', productCode]);
    return hystrixutil.request('selfproductbenefitprofiling', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
        const responseCode = result && result.data && result.data.result.errorCode ? result.data.result.errorCode : '01';
        if (responseCode === '00') {
            let allowedData;
            if (result.data.result.data.length > 0)
                allowedData = true;
            else
                allowedData = false;
            return allowedData;
        } else {
            return false;
        }
    }).catch(error => {
        hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Get Product Benefit Profiling', message: 'Error Get Product Benefit Profiling', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
        return false;
    });
}

function getDetail(req, data) {
    const getDetail = [];
    const { product_name_mccm } = data;
    const result = [];
    getDetail.push(getProductBenefitProfilling(req, product_name_mccm));
    return Promise.all(getDetail).then(details => {
        Object.assign(data, { view_details: details[0] });
        result.push(data);
        return result;
    });
}

function encryptProductCode(req, productCode, description) {
    const authHeader = req.headers.authorization;
    const token = authHeader.replace('Bearer ', '');
    const timestamp = new Date().getTime();
    const encbId = indirectObject.encrypt(beCipher, timestamp + '|' + productCode + '|' + token + '|' + description, authHeader);
    return encbId;
}

function doGetNboPackageWebsite(req, res) {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request NBO Package' }));
    const lang = req.headers.language;
    const { msisdn } = req.params;

    getPrefix(req, msisdn).then(result => {
        if (result.errorCode === '00') {
            let channel;
            const brand = result.data;
            if (result && result.data === 'XL') {
                channel = 'SIDOMPUL_XLPRE';
            } else if (result && result.data === 'AXIS') {
                channel = 'SIDOMPUL_AXIS';
            }
            getNBOWebsite(req, channel, brand).then(nbos => {
                if (nbos && nbos.length > 0) {
                    // takeout offer when value is NA
                    let i = nbos.length;
                    while (i--) {
                        if (nbos[i].product_name === 'NA' || nbos[i].dompul_price === 'NA' || nbos[i].product_name_mccm === 'NA') {
                            nbos.splice(i, 1);
                        }
                    }
                    if (nbos && nbos.length > 0) {
                        nbos.forEach(elm => {
                            elm.product_name_mccm = encryptProductCode(req, elm.product_name_mccm, elm.product_name);
                        });
                        res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: nbos });
                    } else {
                        res.sendNotFound({ errorCode: rc.codes['15'].rc, errorMessage: rc.i18n('15', lang).rm });
                    }
                } else {
                    res.sendNotFound({ errorCode: rc.codes['15'].rc, errorMessage: rc.i18n('15', lang).rm });
                }
            }).catch(error => {
                hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Check Get NBO', message: 'Error Get NBO', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
                res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
            });
        } else {
            hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
            res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
        }
    }).catch(error => {
        hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Check Get Prefix', message: 'Error Get Prefix', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
        res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
    });
}

function getNBOWebsite(req, channel, brand) {
    return doGetNBOWebsite(req, channel, brand).then(nbos => {
        const data = [...nbos];
        if (data.length > 0) {
            const promises = [];
            data.forEach(elm => {
                promises.push(getDetailWebsite(req, elm));
            });
            return Promise.all(promises).then(nboDetails => {
                return nboDetails.reduceRight(
                    (accumulator, currentValue) => accumulator.concat(currentValue)
                );
            });
        }
    });
}

function doGetNBOWebsite(req, channel, brand) {
    const { user } = req;
    const { msisdn, location, maxOffer } = req.params;
    const authHeader = req.headers.authorization;
    const fallbackOptions = {};
    const requestOptions = jClone.clone(config.apis['getnbov2']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = authHeader.startsWith('Bearer ') ? user.msisdn : '';
    const querys = [];
    querys.push(['msisdn', msisdn]);
    querys.push(['channel', channel]);
    querys.push(['location', location]);
    querys.push(['ax-request-id', uuidv1()]);

    // const data = '{"data": {"subscriberId":590354082,"msisdn":"6287885148024","offers":{"offer":[{"id":143296,"location":["NBO","Slot1"],"offer_tag":"NBO-SIDOMPUL-XL","channelAttributes":{"channelAttribute":[{"name":"DOMPUL_PRICE","value":"150988"},{"name":"OFFER_DETAIL_TEXT","value":"Anda memilih XTRA Combo 35GB+70GB, 30hr, Disc 30%, Rp167rb"},{"name":"SLOT_TEXT","value":"XC 35GB+70GB, 30hr, 167rb (Normal: 239rb - Disc 30%)"},{"name":"DOMPUL_BANNER_ID","value":"DEFAULT VALUE"},{"name":"CD_MAIN_ID_CHN","value":"TK_CLM_143296"},{"name":"DOMPUL_PKG_TOC","value":"Some Terms and Conditions about Some package"},{"name":"DOMPUL_PKG_DESCRIPTION","value":"Some Description about some package"},{"name":"PRODUCT_CODE","value":"XC3239"}]},"offerAttributes":{"offerAttribute":[{"name":"ACTIVE_PERIOD","value":"30"},{"name":"AMOUNT_0","value":"167000"},{"name":"BENEFIT_PRICE","value":"167000"},{"name":"BENEFIT_PRICE_ATTR_NAME","value":"AMOUNT_0"},{"name":"CD_MAIN_ID","value":"CLM_143296"},{"name":"MIN_BALANCE_0","value":"0"},{"name":"OFFER_TYPE","value":"OFFER"},{"name":"OfferType","value":"SUBSCRIPTION"},{"name":"SERVICE_ID","value":"8212787"},{"name":"SUBSCRIBER_MSISDN_ATTR_NAME","value":"MSIDSN"},{"name":"SUBSCRIPTION_TYPE","value":"VOICEDATASMSOT"},{"name":"TIER_COUNT","value":"1"},{"name":"OFFER_NAME","value":"XTRA Combo 35GB+70GB, 30hr, Rp167rb (Normal: Rp239rb - Disc 30%)"},{"name":"OFFER_DISCOUNT","value":"72000"},{"name":"BENEFIT_NAME","value":"XTRA Combo 35GB+70GB, 30hr, Rp239rb"},{"name":"BENEFIT_LIST_PRICE","value":"239000"},{"name":"BENEFIT_COST","value":"239000"},{"name":"BENEFIT_TYPE_NAME","value":"Subscription"}]}},{"id":179006,"location":["NBO","Slot3"],"offer_tag":"NBO-SIDOMPUL-XL","channelAttributes":{"channelAttribute":[{"name":"OFFER_DETAIL_TEXT","value":"Anda memilih XC Flex XXXL, 30hr, 125rb"},{"name":"DOMPUL_PRICE","value":"119770"},{"name":"DOMPUL_BANNER_ID","value":"DEFAULT VALUE"},{"name":"SLOT_TEXT","value":"XC Flex XXXL, 30hr, 125rb"},{"name":"PRODUCT_CODE","value":"FLEX125"},{"name":"DOMPUL_PKG_DESCRIPTION","value":"Some Description about some package"},{"name":"CD_MAIN_ID_CHN","value":"TK_CLM_179006"},{"name":"DOMPUL_PKG_TOC","value":"Some Terms and Conditions about Some package"}]},"offerAttributes":{"offerAttribute":[{"name":"ACTIVE_PERIOD","value":"30"},{"name":"AMOUNT_0","value":"125000"},{"name":"BENEFIT_PRICE","value":"125000"},{"name":"BENEFIT_PRICE_ATTR_NAME","value":"AMOUNT_0"},{"name":"CD_MAIN_ID","value":"CLM_179006"},{"name":"MIN_BALANCE_0","value":"0"},{"name":"OFFER_TYPE","value":"OFFER"},{"name":"OfferType","value":"SUBSCRIPTION"},{"name":"SERVICE_ID","value":"8213677"},{"name":"SUBSCRIBER_MSISDN_ATTR_NAME","value":"MSIDSN"},{"name":"SUBSCRIPTION_TYPE","value":"VOICEDATASMSOT"},{"name":"TIER_COUNT","value":"1"},{"name":"OFFER_NAME","value":"XC Flex XXXL, 30hr, 125rb"},{"name":"OFFER_DISCOUNT","value":"0"},{"name":"BENEFIT_NAME","value":"XC Flex XXXL, 30hr, 125rb"},{"name":"BENEFIT_LIST_PRICE","value":"125000"},{"name":"BENEFIT_COST","value":"125000"},{"name":"BENEFIT_TYPE_NAME","value":"Subscription"}]}}]}}}';
    // const result = Promise.resolve(JSON.parse(data));
    // return result.then(result => {
    return hystrixutil.request('getnbov2', requestOptions, apigatehttp.request, fallbackOptions, fallback, querys).then(result => {
        const offers = result && result.data && result.data.offers && result.data.offers.offer ? result.data.offers.offer : [];
        const response = [];
        if (offers) {
            if (Array.isArray(offers) && offers.length > 0) {
                for (let i = 0; i < offers.length; i++) {
                    const offer = offers[i];
                    const channelAttributes = offer.channelAttributes.channelAttribute;
                    const offerAttributes = offer.offerAttributes.offerAttribute;
                    const attributes = [channelAttributes, offerAttributes];
                    const exposedAttributes = ['SLOT_TEXT', 'BENEFIT_PRICE', 'PRODUCT_CODE'];
                    const object = {};
                    for (let i = 0; i < attributes.length; i++) {
                        for (let j = 0; j < attributes[i].length; j++) {
                            if (exposedAttributes.includes(attributes[i][j].name)) {
                                object[attributes[i][j].name] = attributes[i][j].value;
                            }
                        }
                    }
                    const nbo = {
                        brand,
                        product_name: object.SLOT_TEXT,
                        price: object.BENEFIT_PRICE,
                        product_name_mccm: object.PRODUCT_CODE,
                        offer_id: offer.id.toString(),
                    };
                    response.push(nbo);
                }
            }
            if (response.length > maxOffer) {
                response.splice(maxOffer, response.length - maxOffer)
            }
            // reverse sort to original from mccm
            response.reverse();
        }
        return response;
    }).catch(error => {
        hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Get NBO', message: 'Error Get NBO', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
        return [];
    });
    // });
}

function getDetailWebsite(req, data) {
    const getDetail = [];
    const { product_name_mccm } = data;
    const result = [];
    getDetail.push(getProductBenefitProfillingWebsite(req, product_name_mccm));
    return Promise.all(getDetail).then(details => {
        Object.assign(data, { view_details: details[0].allowedData ? true : false });
        Object.assign(data, { normal_price: details[0].normal_price });
        result.push(data);
        return result;
    });
}

function getProductBenefitProfillingWebsite(req, productCode) {
    const fallbackOptions = {};
    const fallback = (error, args) => {
        return Promise.reject({
            message: error.message,
            args
        });
    };
    const { user } = req;
    const authHeader = req.headers.authorization;
    const requestOptions = jClone.clone(config.apis['selfproductbenefitprofiling']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = authHeader.startsWith('Bearer ') ? user.msisdn : '';
    const querys = [];
    querys.push(['authHeader', authHeader]);
    querys.push(['productCode', productCode]);
    return hystrixutil.request('selfproductbenefitprofiling', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
        const responseCode = result && result.data && result.data.result.errorCode ? result.data.result.errorCode : '01';
        let allowedData;
        let normal_price;
        if (responseCode === '00') {
            if (result.data.result.data.length > 0) {
                allowedData = true;
                normal_price = result.data.result.data[0].normal_price;
            } else {
                allowedData = false;
                normal_price = "";

            }
            return { allowedData, normal_price };
        }
    }).catch(error => {
        hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Get Product Benefit Profiling', message: 'Error Get Product Benefit Profiling', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
        return false;
    });
}

module.exports = {
    getNboPackage: doGetNboPackage,
    postNboOfferShown: doPostNboOfferShown,
    getNboPackageWebsite: doGetNboPackageWebsite,
}