/**
 * @name programprofiling-v2-service
 * @description This module serve content v2 route program profiling.
 */
'use strict';

const hydraExpress = require('hydra-express');
const hystrixutil = require('@xl/hystrix-util');
const config = require('fwsp-config');
const axios = require('axios');
const stringify = require('jsesc');
const jClone = require('@xl/json-clone');
const aes256 = require('@xl/custom-aes256');
const rc = require('../constant/RC');

const beCipher = new aes256();
beCipher.createCipher(config.pin.backend);

function isValid(startDate, endDate) {
	let current = (new Date()).toISOString();
	current = current.substring(0, 19).replace(/[-:T]/g, '');
	const start = startDate ? startDate.substring(0, 19).replace(/[-:T]/g, '') : current;
	const end = endDate ? endDate.substring(0, 19).replace(/[-:T]/g, '') : current;
	if (current >= start && current <= end) {
		return true;
	} else {
		return false;
	}
}

function composeMapData(profilingRule, dataCMS) {
	let mapProfiling = new Map();
	dataCMS.map(obj => {
		const prodDisplay = isValid(obj.start_display, obj.end_display);
		if (prodDisplay) {
			const location = profilingRule.filter(res => res.level === obj.city_level)[0];
			const city = (location && typeof location !== 'undefined') ? location.city : 'DEFAULT';
			obj.location = city;
			let profiling = [obj];
			let keyMap = (obj.city_level !== '') ? obj.city_level : 'null';
			if (obj.city_level !== '' && mapProfiling.has(obj.city_level)) {
				profiling = mapProfiling.get(obj.city_level);
				profiling.push(obj);
			} else if (obj.city_level === '' && mapProfiling.has("null")) {
				profiling = mapProfiling.get("null");
				profiling.push(obj);
			}
			mapProfiling.set(keyMap, profiling);
		}
	});
	return mapProfiling;
}

function composeProgramInfo(profilingRules, dataCMS) {
	const dataProfiling = composeMapData(profilingRules, dataCMS);
	const eligibleProgram = [];
	profilingRules = profilingRules.filter(res => res.status === 'ACTIVE');
	profilingRules.map(profiling => {
		let programProfiling = dataProfiling.get(profiling.level);
		const programID = (profiling.program_id && typeof profiling.program_id !== 'undefined' && profiling.program_id.length > 0) ? profiling.program_id.replace(/ /g, '').split(',') : [];
		if (programProfiling && typeof programProfiling !== 'undefined') {
			if (programID.length > 0) {
				eligibleProgram.push(...programProfiling.filter(res => programID.includes(res.news_id)));
			} else {
				eligibleProgram.push(...programProfiling);
			}
		}
	});
	const nullProgramProfiling = dataProfiling.get("null");
	if (nullProgramProfiling && typeof nullProgramProfiling !== 'undefined') {
		eligibleProgram.push(...nullProgramProfiling);
	}
	return eligibleProgram;
}

function composeBannerProfiling(profilingRules, dataCMS) {
	const dataProfiling = composeMapData(profilingRules, dataCMS);
	const eligibleBanner = [];
	profilingRules = profilingRules.filter(res => res.status === 'ACTIVE');
	profilingRules.map(profiling => {
		let bannerProfiling = dataProfiling.get(profiling.level);
		const bannerID = (profiling.banner_id && typeof profiling.banner_id !== 'undefined' && profiling.banner_id.length > 0) ? profiling.banner_id.replace(/ /g, '').split(',') : [];
		if (bannerProfiling && typeof bannerProfiling !== 'undefined') {
			if (bannerID.length > 0) {
				eligibleBanner.push(...bannerProfiling.filter(res => bannerID.includes(res.news_id)));
			} else {
				eligibleBanner.push(...bannerProfiling);
			}
		}
	});
	const nullBannerProfiling = dataProfiling.get("null");
	if (nullBannerProfiling && typeof nullBannerProfiling !== 'undefined') {
		eligibleBanner.push(...nullBannerProfiling);
	}
	return eligibleBanner;
}

module.exports = {
	getMsisdnCacheToggle: () => {
		return function (req, res) {
			return req.user && req.user.msisdn;
		}
	},
	msisdnCacheOptions: {
		appendKey: (req, res) => {
			return req.user && req.user.msisdn ? req.user.msisdn : req.client.client_id;
		},
		headers: {
			'cache-control': 'no-cache, no-store, must-revalidate',
			'pragma': 'no-cache',
			'expires': 0
		}
	},
	cacheOptions: {
		appendKey: (req, res) => {
			return req.user && req.user.msisdn ? req.user.msisdn : req.client.client_id;
		},
		headers: {
			'cache-control': 'no-cache, no-store, must-revalidate',
			'pragma': 'no-cache',
			'expires': 0
		}
	},
	getCacheDurationInMillis: () => {
		const today = new Date();
		const seconds = today.getSeconds();
		const minutesToSeconds = today.getMinutes() * 60;
		const hoursToSeconds = today.getHours() * 60 * 60;
		const result = hoursToSeconds + minutesToSeconds + seconds;
		return (86400 - result) * 1000;
	},
	getMiniRoProgram: (req, res) => {
		const lang = req.headers.language;
		const requests = [];
		hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request for RO Program List API' }));
		requests.push(module.exports.getProfilingProgramRule(req));
		requests.push(module.exports.getProgramCMS(req));
		Promise.all(requests).then(results => {
			const profilingRules = results[0];
			const dataCMS = results[1];
			const eligibleProgram = composeProgramInfo(profilingRules, dataCMS);
			res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: eligibleProgram });
		}).catch(error => {
			hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Call API combined program info', message: 'Error calling API combined program info', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
			hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
			res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
		});
	},
	getMiniRoProgramDetail: (req, res) => {
		const lang = req.headers.language;
		const requests = [];
		hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request for RO Program Detail API' }));
		requests.push(module.exports.getProfilingProgramRule(req));
		requests.push(module.exports.getProgramDetailCMS(req));
		Promise.all(requests).then(results => {
			const profilingRules = results[0];
			const dataCMS = results[1];
			const eligibleProgram = composeProgramInfo(profilingRules, dataCMS);
			res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: eligibleProgram });
		}).catch(error => {
			hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Call API program info detail', message: 'Error calling API program info detail', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
			hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
			res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
		});
	},
	getMiniRoBanner: (req, res) => {
		const lang = req.headers.language;
		const requests = [];
		hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request for RO Banner API' }));
		requests.push(module.exports.getProfilingBannerRule(req));
		requests.push(module.exports.getBannerCMS(req));
		Promise.all(requests).then(results => {
			const profilingRules = results[0];
			const dataCMS = results[1];
			const eligibleBanner = composeBannerProfiling(profilingRules, dataCMS);
			res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: eligibleBanner });
		}).catch(error => {
			hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Call API combined banner profiling', message: 'Error calling API combined banner profiling', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
			hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
			res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
		});
	},
	getMiniRoBannerDetail: (req, res) => {
		const lang = req.headers.language;
		const requests = [];
		hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request for RO Banner Detail API' }));
		requests.push(module.exports.getProfilingBannerRule(req));
		requests.push(module.exports.getBannerDetailCMS(req));
		Promise.all(requests).then(results => {
			const profilingRules = results[0];
			const dataCMS = results[1];
			const eligibleBanner = composeBannerProfiling(profilingRules, dataCMS);
			res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: eligibleBanner });
		}).catch(error => {
			hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Call API banner detail', message: 'Error calling API banner detail', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
			hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
			res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
		});
	},
	getProfilingProgramRule: (req) => {
		const { user } = req;
		const fallbackOptions = {};
		const fallback = (error, args) => {
			return Promise.reject({
				message: error.message,
				args
			});
		};
		const authHeader = req.headers.authorization;
		const requestOptions = jClone.clone(config.apis['selfrominiprofilingprogramrule']);
		requestOptions.headers.event_id = req.logId;
		requestOptions.headers.actor = user.msisdn;
		const querys = [];
		querys.push(['authHeader', authHeader]);
		return hystrixutil.request('selfrominiprofilingprogramrule', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
			hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Call API profiling program rule', message: 'Calling API profiling program rule success', data: result.data }));
			const data = result && result.data && result.data.result ? result.data.result : [];
			return data;
		}).catch(error => {
			hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Call API profiling program rule', message: 'Error calling API profiling program rule', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
			return [];
		});
	},
	getProfilingBannerRule: (req) => {
		const { user } = req;
		const fallbackOptions = {};
		const fallback = (error, args) => {
			return Promise.reject({
				message: error.message,
				args
			});
		};
		const authHeader = req.headers.authorization;
		const requestOptions = jClone.clone(config.apis['selfrominiprofilingbannerrule']);
		requestOptions.headers.event_id = req.logId;
		requestOptions.headers.actor = user.msisdn;
		const querys = [];
		querys.push(['authHeader', authHeader]);
		return hystrixutil.request('selfrominiprofilingbannerrule', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
			hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Call API profiling banner rule', message: 'Calling API profiling banner rule success', data: result.data }));
			const data = result && result.data && result.data.result ? result.data.result : [];
			return data;
		}).catch(error => {
			hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Call API profiling banner rule', message: 'Error calling API profiling banner rule', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
			return [];
		});
	},
	getProgramCMS: (req) => {
		const { user } = req;
		const fallbackOptions = {};
		const fallback = (error, args) => {
			return Promise.reject({
				message: error.message,
				args
			});
		};
		const authHeader = req.headers.authorization;
		const requestOptions = jClone.clone(config.apis['rominiprogramlist']);
		requestOptions.headers.event_id = req.logId;
		requestOptions.headers.actor = user.msisdn;
		const querys = [];
		querys.push(['authHeader', authHeader]);
		return hystrixutil.request('rominiprogramlist', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
			hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Call API Mini RO Program List', message: 'Calling API Mini RO Program List success', data: result.data }));
			const data = result && result.data ? result.data : [];
			return data;
		}).catch(error => {
			hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Call API Mini RO Program List', message: 'Error calling API Mini RO Program List', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
			return [];
		});

	},
	getProgramDetailCMS: (req) => {
		const { user } = req;
		const { newsId } = req.params;
		const fallbackOptions = {};
		const fallback = (error, args) => {
			return Promise.reject({
				message: error.message,
				args
			});
		};
		const authHeader = req.headers.authorization;
		const requestOptions = jClone.clone(config.apis['rominiprogramdetail']);
		requestOptions.headers.event_id = req.logId;
		requestOptions.headers.actor = user.msisdn;
		const querys = [];
		querys.push(['authHeader', authHeader]);
		querys.push(['news-id', newsId]);
		return hystrixutil.request('rominiprogramdetail', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
			hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Call API Mini RO Program Detail', message: 'Calling API Mini RO Program Detail success', data: result.data }));
			const data = result && result.data ? result.data : [];
			return data;
		}).catch(error => {
			hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Call API Mini RO Program Detail', message: 'Error calling API Mini RO Program Detail', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
			return [];
		});
	},
	getBannerCMS: (req) => {
		const { user } = req;
		const fallbackOptions = {};
		const fallback = (error, args) => {
			return Promise.reject({
				message: error.message,
				args
			});
		};
		const authHeader = req.headers.authorization;
		const requestOptions = jClone.clone(config.apis['rominibanner']);
		requestOptions.headers.event_id = req.logId;
		requestOptions.headers.actor = user.msisdn;
		const querys = [];
		querys.push(['authHeader', authHeader]);
		return hystrixutil.request('rominibanner', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
			hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Call API profiling banner rule', message: 'Calling API profiling banner rule success', data: result.data }));
			const data = result && result.data ? result.data : [];
			return data;
		}).catch(error => {
			hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Call API profiling banner rule', message: 'Error calling API profiling banner rule', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
			return [];
		});
	},
	getBannerDetailCMS: (req) => {
		const { user } = req;
		const { newsId } = req.params;
		const fallbackOptions = {};
		const fallback = (error, args) => {
			return Promise.reject({
				message: error.message,
				args
			});
		};
		const authHeader = req.headers.authorization;
		const requestOptions = jClone.clone(config.apis['rominibannerdetail']);
		requestOptions.headers.event_id = req.logId;
		requestOptions.headers.actor = user.msisdn;
		const querys = [];
		querys.push(['authHeader', authHeader]);
		querys.push(['news-id', newsId]);
		return hystrixutil.request('rominibannerdetail', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
			hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Call API Mini RO Banner Detail', message: 'Calling API Mini RO Banner Detail success', data: result.data }));
			const data = result && result.data ? result.data : [];
			return data;
		}).catch(error => {
			hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Call API Mini RO Banner Detail', message: 'Error calling API Mini RO Banner Detail', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
			return [];
		});
	},
}