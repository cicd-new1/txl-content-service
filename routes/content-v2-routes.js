/**
 * @name content-v2-api
 * @description This module packages the Content API.
 */
'use strict';

const hydraExpress = require('hydra-express');
const express = hydraExpress.getExpress();
const authenticator = require('@xl/txl-remote-authenticate');
const logger = require('@xl/txl-logger-handler');
const apicache = require('@xl/apicache');
const contentService = require('../services/content-v2-service');
const productBenefitService = require('../services/productbenefit-v2-service');
const personalHomepageService = require('../services/personalhomepage-v2-service');
const productCatalogService = require('../services/productcatalog-v2-service');
const productProfilingService = require('../services/productprofiling-v2-service');
const bannerprofilingService = require('../services/bannerprofiling-v2-service');
const programprofilingService = require('../services/programprofiling-v2-service');
const roCareService = require('../services/rocare-v2-service');
const extendStockService = require('../services/extendstock-v2-service');
const validator = require('@xl/input-validator');
const nboService = require('../services/nbo-v2-service');

const cache = apicache.middleware;

const api = express.Router();



api.get('/personalhomepage', logger.countEvent('txl-content-service/v2/content/personalhomepage'), authenticator.remote('Bearer'), contentService.encryptedObjectHandler, cache('1 day', personalHomepageService.getMsisdnCacheToggle, personalHomepageService.cacheOptions), bannerprofilingService.getPersonalHomepage);

api.get('/productbenefit', logger.countEvent('txl-content-service/v2/content/productbenefit'), authenticator.remote('Bearer'), contentService.encryptedObjectHandler, cache('1 month'), productBenefitService.getProductAndIconBenefit);

api.get('/api/:apiName', logger.countEvent('txl-content-service/v2/content/api'), authenticator.remote('Bearer'), contentService.encryptedObjectHandler, productCatalogService.getProductCatalog);

api.get('/productsearch/:msisdn/:keyword', logger.countEvent('txl-content-service/v2/content/productsearch'), authenticator.remote('Bearer'), validator.validate([['req.params.msisdn', validator.MSISDN_TYPE]]), contentService.encryptedObjectHandler, productCatalogService.getProductSearch);

api.get('/profiling/:apiName', logger.countEvent('txl-content-service/v2/content/profiling'), authenticator.remote('Bearer'), productProfilingService.checkAccessLevel, contentService.encryptedObjectHandler, cache('1 day', productProfilingService.getMsisdnCacheToggle, productProfilingService.cacheOptions), productProfilingService.getListV2);

api.get('/profiling-productbenefit', logger.countEvent('txl-content-service/v2/content/profiling/productbenefit'), authenticator.remote('Both'), contentService.encryptedObjectHandler, contentService.decryptedObjectHandler, cache('1 week', productProfilingService.getMsisdnCacheToggle, productProfilingService.cacheOptions), productProfilingService.getProductAndIconBenefitv2);

api.get('/profiling-program/:apiName', logger.countEvent('txl-content-service/v2/content/profiling/roprogramlist'), authenticator.remote('Bearer'), contentService.encryptedObjectHandler, cache('1 day', programprofilingService.getMsisdnCacheToggle, programprofilingService.cacheOptions), programprofilingService.getRoProgram);

api.get('/personal-detail', logger.countEvent('txl-content-service/v2/content/personal-detail'), authenticator.remote('Bearer'), contentService.encryptedObjectHandler, cache('1 day', personalHomepageService.getMsisdnCacheToggle, personalHomepageService.cacheOptions), bannerprofilingService.getNewsDetail);

api.get('/extend-stock/:apiName', logger.countEvent('txl-content-service/v2/content/extend-stock'), authenticator.remote('Bearer'), contentService.encryptedObjectHandler, cache('1 day', extendStockService.getMsisdnCacheToggle, extendStockService.cacheOptions), extendStockService.getExtendStock);

api.get('/faq-login', logger.countEvent('txl-content-service/v2/content/faq-login'), authenticator.remote('Basic'), contentService.encryptedObjectHandler, cache('1 month'), roCareService.getLoginFAQ);

api.get('/next-best-offer/:msisdn/:location/:maxOffer', logger.countEvent('txl-content-service/v2/content/next-best-offer'), authenticator.remote('Bearer'), nboService.getNboPackage);

api.post('/next-best-offer/shown/:msisdn/:location/:offerId', logger.countEvent('txl-content-service/v2/content/next-best-offer/shown'), authenticator.remote('Both'), nboService.postNboOfferShown);

api.get('/rocare/faq', logger.countEvent('txl-content-service/v2/content/rocare/faq'), authenticator.remote('Bearer'), contentService.encryptedObjectHandler, roCareService.getSearchFAQ);

api.get('/rocare/mini-ro/faq', logger.countEvent('txl-content-service/v2/content/rocare/mini-ro/faq'), authenticator.remote('Bearer'), contentService.encryptedObjectHandler, roCareService.getFaqMiniRO);

api.get('/next-best-offer-website/:msisdn/:location/:maxOffer', logger.countEvent('txl-content-service/v2/content/next-best-offer-website'), authenticator.remote('Basic'), nboService.getNboPackageWebsite);

module.exports = api;