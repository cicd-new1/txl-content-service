/**
 * @name ro-mini-v2-api
 * @description This module packages the Content API.
 */
'use strict';

const hydraExpress = require('hydra-express');
const express = hydraExpress.getExpress();
const authenticator = require('@xl/txl-remote-authenticate');
const logger = require('@xl/txl-logger-handler');
const apicache = require('@xl/apicache');
const contentService = require('../services/content-v2-service');
const roMiniService = require('../services/ro-mini-v1-service');

const cache = apicache.middleware;

const api = express.Router();



api.get('/profiling-program', logger.countEvent('txl-content-service/v1/ro-mini/profiling/roprogramlist'), authenticator.remote('Bearer'), contentService.encryptedObjectHandler, cache('1 day', roMiniService.getMsisdnCacheToggle, roMiniService.cacheOptions), roMiniService.getMiniRoProgram);

api.get('/profiling-program-detail/:newsId', logger.countEvent('txl-content-service/v1/ro-mini/profiling/roprogramdetail'), authenticator.remote('Bearer'), contentService.encryptedObjectHandler, cache('1 day', roMiniService.getMsisdnCacheToggle, roMiniService.cacheOptions), roMiniService.getMiniRoProgramDetail);

api.get('/profiling-banner', logger.countEvent('txl-content-service/v1/ro-mini/profiling/banner'), authenticator.remote('Bearer'), contentService.encryptedObjectHandler, cache('1 day', roMiniService.getMsisdnCacheToggle, roMiniService.cacheOptions), roMiniService.getMiniRoBanner);

api.get('/profiling-banner-detail/:newsId', logger.countEvent('txl-content-service/v1/ro-mini/profiling/banner/detail'), authenticator.remote('Bearer'), contentService.encryptedObjectHandler, cache('1 day', roMiniService.getMsisdnCacheToggle, roMiniService.cacheOptions), roMiniService.getMiniRoBannerDetail);


module.exports = api;