/**
 * @name content-v1-api
 * @description This module packages the Content API.
 */
'use strict';

const hydraExpress = require('hydra-express');
const hydra = hydraExpress.getHydra();
const express = hydraExpress.getExpress();
const authenticator = require('@xl/txl-remote-authenticate');
const hystrixutil = require('@xl/hystrix-util');
const masheryhttp = require('@xl/mashery-http');
const config = require('fwsp-config');
const axios = require('axios');
const stringify = require('jsesc');
const logger = require('@xl/txl-logger-handler');
const jClone = require('@xl/json-clone');
const aes256 = require('@xl/custom-aes256');
const indirectObject = require('@xl/indirect-object-handler');
const validator = require('@xl/input-validator');
const apicache = require('@xl/apicache');
const rc = require('../constant/RC');
const Moment = require('moment');
const MomentRange = require('moment-range');
const apigatehttp = require('@xl/apigate-http');

const cache = apicache.middleware;

const api = express.Router();

const beCipher = new aes256();
beCipher.createCipher(config.pin.backend);

const msisdnCacheOptions = {
  appendKey: (req, res) => {
    return req.user && req.user.msisdn ? req.user.msisdn : req.client.client_id;
  },
  headers: {
    'cache-control': 'no-cache, no-store, must-revalidate',
    'pragma': 'no-cache',
    'expires': 0
  }
}

const msisdnCacheToggle = (req, res) => {
  return req.user && req.user.msisdn;
}



api.get('/api/:apiName', logger.countEvent('txl-content-service/v1/content/api'), authenticator.remote('Both'), checkAccessLevel, encryptedObjectHandler, cache(getCacheDurationInMillis, msisdnCacheToggle, msisdnCacheOptions), (req, res) => {
  hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Request for common content API'}));
  /*
  req.user = {
    msisdn: '6287780490413',
    parent_account_cd: '90057197'
  }
  */
  const lang = req.headers.language;
  const user = req.user ? req.user : {};
  const { apiName } = req.params;
  const queryString = req.query;
  const keys = Object.keys(queryString);
  const querys = [];
  for(let i=0; i< keys.length; i++) {
    const key = keys[i];
    if(!(key === 'parent-account-cd' || key === 'account-cd' || key === 'dealer-id')) {
      querys.push([key, queryString[key]]);
    }
  }
  querys.push(['parent-account-cd', user.parent_account_cd]);
  querys.push(['account-cd', user.account_cd]);
  querys.push(['dealer-id', user.dealer_id]);
  if (apiName === 'productflashsale') {
    const authHeader = req.headers.authorization;
    const selfApiName = 'self'.concat(apiName);
    const fallbackOptions = {};
    const requestOptions = jClone.clone(config.apis[selfApiName]);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user && user.msisdn ? user.msisdn : null;
    querys.push(['authHeader', authHeader]);

    hystrixutil.request(selfApiName, requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
      let allowedData = [];
      if (result && result.data && result.data.result && result.data.result.data) {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Call self API product catalog', message: 'Calling self API product catalog success', data: result.data.result.data}));
        allowedData = result.data.result.data;
      }
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Response success', data: allowedData}));
      res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: allowedData});
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call self API product catalog', message: 'Error calling self API product catalog', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
      res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
    });
  } else {
    // to handle auth with basic token
    const authHeader = req.headers.authorization;
    user.parent_account_cd = authHeader.startsWith('Bearer ') ? user.parent_account_cd : '90170010';
    
    const fallbackOptions = {};
    const requestOptions = jClone.clone(config.apis[apiName]);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user && user.msisdn ? user.msisdn : null;
    hystrixutil.request(apiName, requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Call API '+ apiName, message: 'Call API '+ apiName +' success'}));
      if(Array.isArray(result.data)) {
        let allowedData = [];
        if(result.data.length > 0 && result.data[0].start_display && result.data[0].end_display) {
          result.data.forEach(elm => {
            const prodDisplay = isValid(elm.start_display, elm.end_display);
            const parentAccountCds = elm.parent_account_cd ? elm.parent_account_cd.replace(/ /g, '').split(',') : [user.parent_account_cd];
            const sanityDisplay = isValid(elm.sanity_start_display, elm.sanity_end_display);
            const sanityParentAccountCds = elm.sanity_parent_account_cd ? elm.sanity_parent_account_cd.replace(/ /g, '').split(',') : [];
            const whitelistType = elm.whitelisted_type;
            const filter = whitelistType ? require('../config/filters/'+ whitelistType) : null;
            const filterParam = elm.whitelisted_param;
            const isFiltered = filter && filterParam ? filter.whitelist.includes(getRequestParamValueByString(req, filterParam)) && !filter.blacklist.includes(getRequestParamValueByString(req, filterParam)) : true;
            if(((prodDisplay && parentAccountCds.includes(user.parent_account_cd)) || (sanityDisplay && sanityParentAccountCds.includes(user.parent_account_cd))) && isFiltered) {
              //delete elm.start_display;
              //delete elm.end_display;
              delete elm.sanity_start_display;
              delete elm.sanity_end_display;
              delete elm.parent_account_cd;
              delete elm.sanity_parent_account_cd;
              delete elm.whitelisted_type;
              delete elm.whitelisted_param;
              allowedData.push(elm);
              markMCCMPersonalizedBannerAsRead(req, elm, querys);
            }
          });
        } else {
          result.data.forEach(elm => {
            delete elm.start_display;
            delete elm.end_display;
            delete elm.sanity_start_display;
            delete elm.sanity_end_display;
            delete elm.parent_account_cd;
            delete elm.sanity_parent_account_cd;
            delete elm.whitelisted_type;
            delete elm.whitelisted_param;
          });
          allowedData = result.data;
        }
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Response success', data: allowedData}));

        // Check Eligible Tenure Package
        if (apiName === 'productitemlist' || apiName === 'productitemlistv2') {
          const requests = [];
          requests.push(getTenure(req));
          Promise.all(requests).then(results => {
            const tenure = results[0] ? results[0] : [];
            const eligibleProductTenure = [];
            if (allowedData && allowedData.length > 0) {
              allowedData.forEach(elm => {
                if (!elm.tenure) {
                  eligibleProductTenure.push(elm);
                } else {
                  const tenureCMS = parseInt(elm.tenure, 10);
                  if (tenure && tenure.errorCode === '00'
                    && diffDays(formatDate(tenure.data), formatDate(Date.now())) <= tenureCMS) {
                    eligibleProductTenure.push(elm);
                  }
                }
              });
            }
            res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: eligibleProductTenure });
          }).catch(error => {
            hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Call Promise Profiling Product', message: 'Error calling Promise Profiling Product', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
            hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
            res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
          });
        } else {
          res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: allowedData });
        }
      } else {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Response failed'}));
        res.sendNotFound({errorCode: rc.codes['12'].rc, errorMessage: rc.i18n('12', lang).rm});
      }
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call API '+ apiName, message: 'Error call API '+ apiName, error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
      res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
    });
  }
});

function getCacheDurationInMillis() {
  const today = new Date();
  const seconds = today.getSeconds();
  const minutesToSeconds = today.getMinutes() * 60;
  const hoursToSeconds = today.getHours() * 60 * 60;
  const result = hoursToSeconds + minutesToSeconds + seconds;
  return (86400 - result) * 1000;
}

function isValid(startDate, endDate) {
  let current = (new Date()).toISOString();
  current = current.substring(0, 19).replace(/[-:T]/g, '');
  const start = startDate ? startDate.substring(0, 19).replace(/[-:T]/g, '') : current;
  const end = endDate ? endDate.substring(0, 19).replace(/[-:T]/g, '') : current;
  if(current >= start && current <= end) {
    return true;
  } else {
    return false;
  }
}

api.get('/productsearch/:msisdn/:keyword', logger.countEvent('txl-content-service/v1/content/productsearch'), authenticator.remote('Bearer'), validator.validate([['req.params.msisdn', validator.MSISDN_TYPE]]), encryptedObjectHandler, cache('1 month'), (req, res) => {
  hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request for product search API' }));
  const { user } = req;
  /*
    const fallbackOptions = {};
    let brand = 'XL';
    const requestOptions = jClone.clone(config.apis['prefix']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;
    hystrixutil.request('prefix', requestOptions, masheryhttp.request, fallbackOptions, fallback, [['msisdn', req.params.msisdn]]).then(result => {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Call API prefix', message: 'Call API prefix success'}));
      brand = result && result.data && result.data && result.data.owner ? result.data.owner : 'XL';
      filterProductSearch(req, res, brand);
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call API prefix', message: 'Error calling API prefix', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
      filterProductSearch(req, res, brand);
    });
  */
  const lang = req.headers.language;
  const querys = [];
  const { msisdn, keyword } = req.params;
  const authHeader = req.headers.authorization;
  const selfApiName = 'selfproductsearch';
  const fallbackOptions = {};
  const requestOptions = jClone.clone(config.apis[selfApiName]);
  requestOptions.headers.event_id = req.logId;
  requestOptions.headers.actor = user && user.msisdn ? user.msisdn : null;
  querys.push(['authHeader', authHeader]);
  querys.push(['msisdn', msisdn]);
  querys.push(['keyword', keyword]);

  hystrixutil.request(selfApiName, requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
    let allowedData = [];
    if (result && result.data && result.data.result && result.data.result.data) {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Call self API product search', message: 'Calling self API product search success', data: result.data.result.data }));
      allowedData = result.data.result.data;
    }
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Response success', data: allowedData }));
    res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: allowedData });
  }).catch(error => {
    hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Call self API product search', message: 'Error calling self API product search', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
    res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
  });
});

api.get('/personalhomepage', logger.countEvent('txl-content-service/v1/content/personalhomepage'), authenticator.remote('Bearer'), encryptedObjectHandler, cache('1 day', msisdnCacheToggle, msisdnCacheOptions), (req, res) => {
  hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Request for personal homepage API'}));
  /*
  req.user = {
    msisdn: '6287785475398',
    parent_account_cd: '90057197'
  }
  */
  const lang = req.headers.language;
  let maxBanner = req.query.maxbanner ? req.query.maxbanner : 6;
  const data = [];
  const requests = [];
  requests.push(getCMSPersonalHomepage(req, res));
  requests.push(getMCCMPersonalHomepage(req, res));
  Promise.all(requests).then(results => {
    const cmsBanners = results[0] ? results[0] : [];
    const mccmBanners = results[1] ? results[1] : [];
    const temp = [...cmsBanners, ...mccmBanners];
    temp.sort((a,b) => (a.banner_position > b.banner_position) ? 1 : ((b.banner_position > a.banner_position) ? -1 : 0));
    maxBanner = maxBanner > temp.length ? temp.length : maxBanner;
    for(let i = 0; i < maxBanner; i++) {
      data.push(temp[i]);
    }
    //res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: data});
    res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data});
  }).catch(error => {
    hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call API combined news homepage', message: 'Error calling API combined news homepage', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
    res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
  });
});

api.get('/personalhomepagev2', logger.countEvent('txl-content-service/v1/content/personalhomepagev2'), authenticator.remote('Bearer'), encryptedObjectHandler, cache('1 day', msisdnCacheToggle, msisdnCacheOptions), (req, res) => {
  hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Request for personal homepage v2 API'}));
  /*
  req.user = {
    msisdn: '6287787237069',
    parent_account_cd: 'WGTEST_MD'
  }
  */
  const lang = req.headers.language;
  let maxBanner = req.query.maxbanner ? req.query.maxbanner : 6;
  const data = [];
  const requests = [];
  requests.push(getCMSPersonalHomepage(req, res));
  requests.push(getMCCMPersonalHomepage(req, res));
  Promise.all(requests).then(results => {
    const cmsBanners = results[0] ? results[0] : [];
    const mccmBanners = results[1] ? results[1] : [];
    const temp = [...cmsBanners, ...mccmBanners];
    temp.sort((a,b) => (a.banner_position > b.banner_position) ? 1 : ((b.banner_position > a.banner_position) ? -1 : 0));
    maxBanner = maxBanner > temp.length ? temp.length : maxBanner;
    for(let i = 0; i < maxBanner; i++) {
      data.push(temp[i]);
    }
    //res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: data});
    res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data});
  }).catch(error => {
    hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call API combined news homepage', message: 'Error calling API combined news homepage', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
    res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
  });
});

function getCMSPersonalHomepage(req, res, newsId, offerId) {
  const { user } = req;
  const authHeader = req.headers.authorization;
  const fallbackOptions = {};
  if(newsId) {
    const requestOptions = jClone.clone(config.apis['selfmccmnewshomepage']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;
    const querys = [];
    querys.push(['news-id', newsId]);
    querys.push(['authHeader', authHeader]);
    return hystrixutil.request('selfmccmnewshomepage', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Call API news detail homepage', message: 'Calling API news detail homepage success'}));
      const data = result && result.data && result.data.result && result.data.result.data && result.data.result.data.length > 0 ? result.data.result.data[0] : [];
      data.offer_id = offerId;
      return data;
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call API news detail homepage', message: 'Error calling API news detail homepage', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
      return [];
    });
  } else {
    const requestOptions = jClone.clone(config.apis['selfnewshomepage']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;
    const querys = [];
    querys.push(['authHeader', authHeader]);
    return hystrixutil.request('selfnewshomepage', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Call API news homepage', message: 'Calling API news homepage success', data: result.data}));
      const data = result && result.data && result.data.result && result.data.result.data ? result.data.result.data : [];
      return data;
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call API news homepage', message: 'Error calling API news homepage', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
      return [];
    });
  }
}

function getMCCMPersonalHomepage(req, res) {
  const { user } = req;
  const fallbackOptions = {};
  const requestOptions = jClone.clone(config.apis['nextbestoffer']);
  requestOptions.headers.event_id = req.logId;
  requestOptions.headers.actor = user.msisdn;
  const querys = [];
  querys.push(['msisdn', user.msisdn]);
  querys.push(['maxOffer', 6]);
  return hystrixutil.request('nextbestoffer', requestOptions, apigatehttp.request, fallbackOptions, fallback, querys).then(result => {
    const offers = result && result.data && result.data.offers && result.data.offers.offer ? result.data.offers.offer : [];
    const homepageBannerPromises = [];
    if(Array.isArray(offers)) {
      for(let i=0; i<offers.length; i++) {
        const offer = offers[i];
        const offerId = offer.id;
        let newsId = null;
        const channelAttributes = offer.channel_attributes.channel_attribute;
        for(let j=0; j<channelAttributes.length; j++) {
          const attribute = channelAttributes[j];
          if(attribute.name === 'DOMPUL_BANNER_ID') {
            newsId = attribute.value;
            break;
          }
        }
        if(newsId) {
          homepageBannerPromises.push(getCMSPersonalHomepage(req, res, newsId, offerId));
        }
      }
    } else {
      const offerId = offers.id;
      let newsId = null;
      const channelAttributes = offers.channel_attributes.channel_attribute;
      for(let j=0; j<channelAttributes.length; j++) {
        const attribute = channelAttributes[j];
        if(attribute.name === 'DOMPUL_BANNER_ID') {
          newsId = attribute.value;
          break;
        }
      }
      if(newsId) {
        homepageBannerPromises.push(getCMSPersonalHomepage(req, res, newsId, offerId));
      }
    }
    return Promise.all(homepageBannerPromises).then(banners => {
      const resultBanners = [];
      for(let i=0; i<banners.length; i++) {
        if(banners[i] && banners[i].news_id) {
          resultBanners.push(banners[i]);
        }
      }
      return resultBanners;
    });
  }).catch(error => {
    hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call API news homepage MCCM', message: 'Error calling API news homepage MCCM', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
    return [];
  });
}

function markMCCMPersonalizedBannerAsRead(req, elm, querys) {
  const user = req.user ? req.user : {};
  let isOfferIdExist = false;
  for(let i=0;i<querys.length;i++) {
    if(querys[i].includes('offer-id')) {
      isOfferIdExist = true;
      break;
    }
  }
  if(isOfferIdExist && elm.mccm_banner && elm.mccm_banner === 'yes') {
    const fallbackOptions = {};
    const requestOptions = jClone.clone(config.apis['mccmmarkasread']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;
    querys.push(['msisdn', user.msisdn]);
    hystrixutil.request('mccmmarkasread', requestOptions, apigatehttp.request, fallbackOptions, fallback, querys).then(response => {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Call API MCCM callback', message: 'Calling API MCCM callback success'}));
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call API MCCM callback', message: 'Error calling API MCCM callback', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
  });
  }
}

function filterProductSearch(req, res, brand) {
  const fallbackOptions = {};
  const lang = req.headers.language;
  const { user } = req;
  const requestOptions = jClone.clone(config.apis['productsearch']);
  requestOptions.headers.event_id = req.logId;
  requestOptions.headers.actor = user.msisdn;
  hystrixutil.request('productsearch', requestOptions, axios, fallbackOptions, fallback, [['keyword', req.params.keyword]]).then(result => {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Call API product search', message: 'Call API product search success'}));
    let allowedData = [];
    if(Array.isArray(result.data) && result.data.length > 0) {
      result.data.forEach(elm => {
        const parentAccountCds = elm.parent_account_cd ? elm.parent_account_cd.replace(/ /g, '').split(',') : [];
        if(parentAccountCds.includes(user.parent_account_cd) && elm.brand === brand) {
          allowedData.push(elm);
        }
      });
    } else {
      allowedData = [];
    }
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Response success', data: allowedData}));
    res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: allowedData});
  }).catch(error => {
    hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call API product search', message: 'Error calling API product search', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
    res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
  });
}

function encryptedObjectHandler(req, res, next) {
  const authHeader = req.headers.authorization;
  const { user } = req;
  if(user) {
    user.parent_account_cd = indirectObject.decrypt(beCipher, user.parent_account_cd, authHeader);
    user.account_cd = indirectObject.decrypt(beCipher, user.account_cd, authHeader);
    user.dealer_id = indirectObject.decrypt(beCipher, user.dealer_id, authHeader);
  }
  if(req.query['parent-account-cd']) {
    const tempParentAccountCd = req.query['parent-account-cd'];
    req.query['parent-account-cd'] = indirectObject.decrypt(beCipher, req.query['parent-account-cd'], authHeader);
    const oriUrl = req.originalClearUrl || req.originalUrl || req.url;
    req.originalClearUrl = oriUrl.replace(encodeURIComponent(tempParentAccountCd), req.query['parent-account-cd']);
  }
  if(req.query['account-cd']) {
    const tempAccountCd = req.query['account-cd'];
    req.query['account-cd'] = indirectObject.decrypt(beCipher, req.query['account-cd'], authHeader);
    const oriUrl = req.originalClearUrl || req.originalUrl || req.url;
    req.originalClearUrl = oriUrl.replace(encodeURIComponent(tempAccountCd), req.query['account-cd']);
  }
  if(req.query['dealer-id']) {
    const tempDealerId = req.query['dealer-id'];
    req.query['dealer-id'] = indirectObject.decrypt(beCipher, req.query['dealer-id'], authHeader);
    const oriUrl = req.originalClearUrl || req.originalUrl || req.url;
    req.originalClearUrl = oriUrl.replace(encodeURIComponent(tempDealerId), req.query['dealer-id']);
  }
  next();
}

function checkAccessLevel(req, res, next) {
  /*
  req.user = {
    msisdn: '6287787237069',
    parent_account_cd: 'WGTEST_MD'
  }
  */
  const authHeader = req.headers.authorization;
  const { apiName } = req.params;
  const msisdn = req.user ? req.user.msisdn : null;

  if(config.apis[apiName] && (config.apis[apiName].accessLevel === 'public' && (authHeader.startsWith('Basic ') || authHeader.startsWith('Bearer ')) || config.apis[apiName].accessLevel === 'protected' && authHeader.startsWith('Bearer ') && msisdn || (config.apis[apiName].accessLevel === 'private' && authenticator.isInternalAccessor(req)))) {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Check registered API', message: 'API '+ apiName +' exists'}));
    next();
  } else {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Check registered API', message: 'API '+ apiName +' not exists'}));
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'API not found'}));
    res.sendNotFound({code: rc.codes['40'].rc, message: rc.codes['40'].rm});
  }
}

function getRequestParamValueByString(req, paramPath) {
	paramPath = paramPath.startsWith('req.') ? paramPath.substring(4) : paramPath;
	const paramPaths = paramPath.split('.');
	let temp = {};
	for(let i=0;i<paramPaths.length;i++) {
		if(i === 0) {
			temp = req[paramPaths[i]];
		} else {
			temp = temp[paramPaths[i]];
		}
	}
	return temp;
}

function fallback(error, args) {
  return Promise.reject({
		message: error.message,
		args
	});
}

function getTenure(req) {
  const fallbackOptions = {};
  const fallback = (error, args) => {
    return Promise.reject({
      message: error.message,
      args
    });
  };
  const bypassForce = req.headers['x-apicache-bypass'] ? req.headers['x-apicache-bypass'] : 'false';
  const { user } = req;
  const bMsisdn = req.query['msisdn-b'] ? req.query['msisdn-b'] : '';
  const authHeader = req.headers.authorization;
  const requestOptions = jClone.clone(config.apis['selfgetsubscriberinfo']);
  requestOptions.headers.event_id = req.logId;
  requestOptions.headers.actor = user.msisdn;
  const querys = [];
  querys.push(['msisdn', bMsisdn]);
  querys.push(['authHeader', authHeader]);
  querys.push(['bypassForce', bypassForce]);
  if (bMsisdn) {
    return hystrixutil.request('selfgetsubscriberinfo', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
      if (result.data.result.data && result.data.result.data.IR_DATE) {
        return { errorCode: '00', data: formatDate(result.data.result.data.IR_DATE) };
      } else {
        return { errorCode: '01', data: [] };
      }
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Get Subscriber Info', message: 'Error Get Subscriber Info', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
      return { errorCode: '01', data: [] };
    });
  } else {
    return { errorCode: '01', data: [] };
  }
}

function formatDate(date) {
  const moment = MomentRange.extendMoment(Moment);
  moment.locale('en');
  return moment(date).format('MM/DD/YYYY');
}

function diffDays(start, end) {
  const date1 = new Date(start);
  const date2 = new Date(end);
  const oneDay = 1000 * 60 * 60 * 24;
  const diffInTime = date2.getTime() - date1.getTime();
  const diffInDays = Math.round(diffInTime / oneDay);
  return diffInDays;
}


module.exports = api;