FROM node:10-alpine as builder

RUN apk --no-cache add python make g++
COPY package*.json ./
RUN npm install -g npm-cli-adduser
RUN npm set registry https://npmrepos.int.dp.xl.co.id/
RUN /usr/local/bin/npm-cli-adduser --registry https://npmrepos.int.dp.xl.co.id/ --scope @xl --username xl --password Mw7WZM7FLHner3kh8tTynNcWsBNqzHEeK --email rachmasaric@xl.co.id
RUN npm install

# The instructions for second stage
FROM node:10-alpine

# set to Jakarta time
RUN apk add tzdata
RUN cp /usr/share/zoneinfo/Asia/Jakarta /etc/localtime
RUN apk del tzdata
RUN npm install -g pm2
# RUN apk add mysql-client busybox-extras curl net-tools
ARG NODE_ENV=NODE_ENVIRONMENT
ENV NODE_ENV=${NODE_ENV}
WORKDIR /usr/src/app
COPY --from=builder node_modules node_modules
COPY . .
RUN rm -rf Dockerfile Jenkinsfile README.md jenkins-*.sh k8s.yml manifest.yml sonar-project.properties .git*
CMD ["pm2-runtime", "starter.js", "-i max"]
EXPOSE 8080
