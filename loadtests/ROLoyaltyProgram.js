import http from 'k6/http';
//import uuid from './uuid.js';

export default function () {
    var url = 'https://tgr-txl-content-service.ext.dp.xl.co.id/v1/content/api/loyaltyprogram'

    var payload = JSON.stringify({
        //desc: 'Gagal Akses SiDOMPUL'
	desc: 'Keluhan Transaksi Gagal',
	//descCategory: 'Voucher Fisik'
	descCategory: 'Dompet Pulsa',
	descSubCategory: 'Saldo Dompul terpotong tidak sesuai dengan transaksi'
        //token: 'bTm0oToo82krNB8uVWDjcQ=='
    });

    var params = {
        headers: {
            //'Content-Type': 'application/json',
            'Authorization': 'Bearer abcd'
        },
        timeout: 10000
    };

    var startTime = new Date();

    //http.post(url, payload, params);
    http.get(url, params);
}
