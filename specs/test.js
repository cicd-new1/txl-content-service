'use strict';

/**
* Change into specs folder so that file loading works.
*/
const chai = require('chai');
const chaiHttp = require('chai-http');
const hydraExpress = require('hydra-express');
const express = hydraExpress.getExpress();
const service = require('../content-service');
const config = require('fwsp-config');

chai.use(chaiHttp);

let testConfig = {
    msisdn: '6287875736033',
    priceplanid: 433598,
    faq_id: '7aebe08e-599f-470f-a370-cd033f58f158',
    package_type: 'POST',
    serviceid: 8211187
};

describe('content-service', function() {

    before(function(done) {
        service.init().then(() => {
            testConfig.basicUrl = 'http://'+ config.hydra.serviceIP +':'+ config.hydra.servicePort,
            done();
        });
    });

    describe('/api', function() {
        it('/priceplanbyid', function(done) {
            chai.request(testConfig.basicUrl)
                .get('/v1/content/api/priceplanbyid?priceplanid='+ testConfig.priceplanid)
                .end((err, res) => {
                    chai.expect(res.status).to.equal(200);
                    chai.expect(res.statusCode).to.equal(200);
                    if(res.body.result[0].priceplan_business_name === null) {
                        chai.expect(res.body.result[0].priceplan_business_name).to.null;
                    } else {
                        chai.expect(res.body.result[0].priceplan_business_name).to.not.null;
                    }
                    done();
                });
        });
        it('/generaltopup', function(done) {
            chai.request(testConfig.basicUrl)
                .get('/v1/content/api/generaltopup')
                .end((err, res) => {
                    chai.expect(res.status).to.equal(200);
                    chai.expect(res.statusCode).to.equal(200);
                    if(res.body.result[0].content_id === null) {
                        chai.expect(res.body.result[0].content_id).to.null;
                    } else {
                        chai.expect(res.body.result[0].content_id).to.not.null;
                    }
                    done();
                });
        });
        it('/amounttopup', function(done) {
            chai.request(testConfig.basicUrl)
                .get('/v1/content/api/amounttopup')
                .end((err, res) => {
                    chai.expect(res.status).to.equal(200);
                    chai.expect(res.statusCode).to.equal(200);
                    if(res.body.result[0].amount_list_title === null) {
                        chai.expect(res.body.result[0].amount_list_title).to.null;
                    } else {
                        chai.expect(res.body.result[0].amount_list_title).to.not.null;
                    }
                    done();
                });
        });
        it('/faqtopic', function(done) {
            chai.request(testConfig.basicUrl)
                .get('/v1/content/api/faqtopic')
                .end((err, res) => {
                    chai.expect(res.status).to.equal(200);
                    chai.expect(res.statusCode).to.equal(200);
                    if(res.body.result[0].topic_id === null) {
                        chai.expect(res.body.result[0].topic_id).to.null;
                    } else {
                        chai.expect(res.body.result[0].topic_id).to.not.null;
                    }
                    done();
                });
        });
        it('/faqdetail', function(done) {
            chai.request(testConfig.basicUrl)
                .get('/v1/content/api/faqdetail?faq_id='+ testConfig.faq_id)
                .end((err, res) => {
                    chai.expect(res.status).to.equal(200);
                    chai.expect(res.statusCode).to.equal(200);
                    if(res.body.result[0].faq_id === null) {
                        chai.expect(res.body.result[0].faq_id).to.null;
                    } else {
                        chai.expect(res.body.result[0].faq_id).to.not.null;
                    }
                    done();
                });
        });
        it('/storelocation', function(done) {
            chai.request(testConfig.basicUrl)
                .get('/v1/content/api/storelocation')
                .end((err, res) => {
                    chai.expect(res.status).to.equal(200);
                    chai.expect(res.statusCode).to.equal(200);
                    if(res.body.result[0].store_name === null) {
                        chai.expect(res.body.result[0].store_name).to.null;
                    } else {
                        chai.expect(res.body.result[0].store_name).to.not.null;
                    }
                    done();
                });
        });
        it('/productbypackagetype', function(done) {
            chai.request(testConfig.basicUrl)
                .get('/v1/content/api/productbypackagetype?package_type='+ testConfig.package_type)
                .end((err, res) => {
                    chai.expect(res.status).to.equal(200);
                    chai.expect(res.statusCode).to.equal(200);
                    if(res.body.result[0].package_id === null) {
                        chai.expect(res.body.result[0].package_id).to.null;
                    } else {
                        chai.expect(res.body.result[0].package_id).to.not.null;
                    }
                    done();
                });
        });
        it('/productbyserviceid', function(done) {
            chai.request(testConfig.basicUrl)
                .get('/v1/content/api/productbyserviceid?serviceid='+ testConfig.serviceid)
                .end((err, res) => {
                    chai.expect(res.status).to.equal(200);
                    chai.expect(res.statusCode).to.equal(200);
                    if(res.body.result[0].package_id === null) {
                        chai.expect(res.body.result[0].package_id).to.null;
                    } else {
                        chai.expect(res.body.result[0].package_id).to.not.null;
                    }
                    done();
                });
        });
        it('/tncaddchild', function(done) {
            chai.request(testConfig.basicUrl)
                .get('/v1/content/api/tncaddchild')
                .end((err, res) => {
                    chai.expect(res.status).to.equal(200);
                    chai.expect(res.statusCode).to.equal(200);
                    if(res.body.result[0].content_id === null) {
                        chai.expect(res.body.result[0].content_id).to.null;
                    } else {
                        chai.expect(res.body.result[0].content_id).to.not.null;
                    }
                    done();
                });
        });
        it('/prefix', function(done) {
            chai.request(testConfig.basicUrl)
                .get('/v1/content/api/prefix')
                .end((err, res) => {
                    chai.expect(res.status).to.equal(200);
                    chai.expect(res.statusCode).to.equal(200);
                    if(res.body.result[0].prefix === null) {
                        chai.expect(res.body.result[0].prefix).to.null;
                    } else {
                        chai.expect(res.body.result[0].prefix).to.not.null;
                    }
                    done();
                });
        });
    });
});
