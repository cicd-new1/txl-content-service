'use strict';

const i18n = require('./i18n.json');

const codes = [];
//expected positive
codes['00'] = {rc: '00', rm: 'Success'};
//expected negative
codes['12'] = {rc: '12', rm: 'Unexpected result'};
codes['15'] = {rc: '15', rm: 'Offer not available'};
codes['40'] = {rc: '40', rm: 'API not found'};
codes['41'] = {rc: '41', rm: 'FAQ not found'};
//abnormal error
codes['91'] = {rc: '91', rm: 'Reach API failed'};

function mapCodes(rc, lang, queries) {
	if(lang == null) {
		lang = 'en';
	}
	let mappedRC = codes[rc] ? codes[rc].rc : '';
	let message = replaceParams(i18n[rc][lang.toLowerCase()], queries);
	message = message == null ? replaceParams(i18n[rc]['en'], queries) : message;
	return {rc: mappedRC, rm: message};
}

function replaceParams(url, params) {
	if(url && params) {
		for (let param in params) {
			url = url.replace('${'+ params[param][0] +'}', params[param][1]);
		}    
	}
	return url;
}
  
module.exports = {
	i18n: mapCodes,
	codes: codes
}